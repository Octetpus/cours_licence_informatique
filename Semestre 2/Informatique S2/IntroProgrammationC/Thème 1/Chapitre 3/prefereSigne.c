#include <stdio.h>

int main()
{
    int nbPref; // declaration
    nbPref=0; // initialisation par sécurité, au cas où le scanf échoue
    printf("Quel est votre entier préféré ? "); // affichage
    scanf("%d", &nbPref); //saisie au clavier, résultat attendu en int, stocké dans nbPref
    if (nbPref>0) // Si la valeur de nbPref et >0
    {
        printf("Il est positif.\n");
    }
    else if (nbPref==0) // Sinon si la valeur est égale à 0
    {
        printf("Il est nul.\n");
    }
    else // Sinon
    {
        printf("Il est négatif.\n");
    }
    
    return 0;
}

#include <stdio.h>

int main()
{
    int nombrePrefere; // declaration de la variable
    nombrePrefere=0; // initialisation
    printf("Bonjour, quel est votre nombre préféré ? "); // affichage
    scanf("%d", &nombrePrefere); // saisie en console, résultat transfomé en int et stocké dans nombrePrefere
    printf("Votre nombre préféré est: %d \n", nombrePrefere);
    printf("Le nombre suivant est: %d \n", nombrePrefere+1);
    
    return 0;
}

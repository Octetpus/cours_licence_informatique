/*=============== TP04 Exo 5 ===============*/
/*===== Jouons avec les chaines de caracteres =======*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void majuscule(char * source, char * destination)
{
    int i=0;
    char lettre;
    do
    {
        lettre=source[i];
        if ((lettre >='a') && (lettre <='z'))
        {
            // il s'agit d'une minuscule
            destination[i]=lettre-32;
        }
        else
        {
            //il s'agit déjà d'une majuscule
            destination[i]=lettre;
        }
        i++;
    } while (lettre!=0); // on s'arrete au caractère '\0', code ASCII 0.
}

void mettrePremierOrdreAlpha(char * destination, char * prenom1, char * nom1, char * prenom2, char * nom2)
{
    if ((strcmp(nom1, nom2)<0) || (strcmp(nom1, nom2)==0 && (strcmp(prenom1, prenom2)<0)))
    {
        sprintf(destination, "%s %s", nom1, prenom1);
    }
    else
    {
        sprintf(destination, "%s %s", nom2, prenom2);
    }
}

int main(int argc, char * argv[])
{
    if (argc==7)
    {
        int age1=atoi(argv[3]);
        int age2=atoi(argv[6]);
        char phrase[100];
        int numPrenom, numNom, age;
        if (age1>age2)
        {
            numPrenom=1;
            numNom=2;
            age=age1;
        }
        else
        {
            numPrenom=4;
            numNom=5;
            age=age2;
            
        }
        char nomMajuscule[20];
        majuscule(argv[numNom], nomMajuscule);
        sprintf(phrase, "Le plus agé est: %s %s, (%d ans)", argv[numPrenom], nomMajuscule, age);
        printf("%s\n", phrase);
        
        char nomMajuscule1[20];
        majuscule(argv[2], nomMajuscule1);
        char nomMajuscule2[20];
        majuscule(argv[5], nomMajuscule2);
        mettrePremierOrdreAlpha(phrase, argv[1], nomMajuscule1, argv[4], nomMajuscule2);
        printf("Premier dans l'ordre alphabetique: %s\n", phrase);
        
    }
    else
    {
        printf("Nombre de paramètres invalide\n");
    }
    return 0;
}

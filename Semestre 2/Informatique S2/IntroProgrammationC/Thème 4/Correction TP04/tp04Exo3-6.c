/* ======== TP04 Exos 3 et 6  =========*/
#include <stdio.h>
#define M 5

/* Fonction auxiliaire d'affichage */
void afficheTab2D(int tab[][M], int n)
{
    int i, j;
    int entree;
    for (i=0; i<n; i++)
    {
        for (j=0; j<M; j++)
        {
            entree=tab[i][j];
            if ((entree >= 0) && (entree<10))
            {
                printf(" "); // pour ameliorer l'affichage
            }
            printf("%d ", entree);
        }
        printf("\n");
    }
    
}


/* Exercice 3 */

int maxTab2D(int tab[][M], int n)
{
    int max=tab[0][0];
    int entree;
    int i,j;
    for (i=0; i<n; i++)
    {
        for (j=0; j<M; j++)
        {
            entree=tab[i][j];
            if (entree>max)
            {
                max=entree;
            }
        }
    }
    return max;
}


/* Exercice 6 */

/* Question a */
int tousEgaux(int tab[][M], int n)
{
    int tousEgaux=1;
    int precedent=tab[0][0];
    int i,j;
    for (i=0; i<n; i++)
    {
        for (j=0; j<M; j++)
        {
            tousEgaux=tousEgaux && (precedent==tab[i][j]);
            precedent=tab[i][j];
        }
    }
    return tousEgaux;
}

/* Question b */
int ligneCroissante(int tab[][M], int n, int ligne)
{
    if (ligne<0 || ligne>=n)
    {
        return -1;
    }
    else
    {
        int croissante=1;
        int precedent=tab[ligne][0];
        int j;
        for (j=0; j<M; j++)
        {
            croissante=croissante && (precedent<=tab[ligne][j]);
            precedent=tab[ligne][j];
        }
        return croissante;
    }
}

/* Question c */
int colonneNegative(int tab[][M], int n, int colonne)
{
    if (colonne<0 || colonne>=M)
    {
        return -1;
    }
    else
    {
        int tousNegatifs=1;
        int i;
        for (i=0; i<n; i++)
        {
            tousNegatifs= tousNegatifs && (tab[i][colonne]<0);
        }
        return tousNegatifs;
    }
}



int main()
{
    int tab[4][M]={ {4, -7, 13, 8, 9},
                    {-8, -1, 5, 13, 17},
                    {14, -7, 23, 8, -5},
                    {4, -7, 13, 8, -9},
                  };
    
    afficheTab2D(tab, 4);
    int m=maxTab2D(tab, 4);
    printf("Max=%d\n", m);
    
    int egaux=tousEgaux(tab, 4);
    printf("Tous egaux: %d\n", egaux);
    
    int t2[10][M];
    int i,j;
    for (i=0; i<10; i++)
    {
        for (j=0; j<M; j++)
        {
            t2[i][j]=5;
        }
    }
    afficheTab2D(t2, 10);
    egaux=tousEgaux(t2, 4);
    printf("Tous egaux: %d\n", egaux);
    
    printf("Ligne croissante tab[1]: %d\n", ligneCroissante(tab, 4, 1));
    printf("Ligne croissante tab[2]: %d\n", ligneCroissante(tab, 4, 2));
    
    printf("Tous negatifs tab[][1]: %d\n", colonneNegative(tab, 4, 1));
    printf("Tous negatifs tab[][4]: %d\n", colonneNegative(tab, 4, 4));
    
    return 0;
}

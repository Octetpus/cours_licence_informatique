/*========= TP04 Exo 2 =============*/
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[])
{
    int i;
    int somme=0;
    for (i=1; i<argc; i++)
    {
        somme=somme+atoi(argv[i]);
        /* bien penser à convertir la chaine argv[i] en int grace à atoi (=ascii to integer) */
    }
    printf("Somme totale: %d\n", somme);
    return 0;
}


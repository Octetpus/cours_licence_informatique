/*========= TP04 Exo 1 =============*/
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
    int i;
    int longueur;
    printf("Valeur de argc: %d\n", argc);
    for (i=0; i<argc; i++)
        /* argc donne la taille du tableau argv (qui est un tableau de chaine de caractères) */
    {
        longueur=strlen(argv[i]); // agrv[i] est une chaine de caracteres
        printf("Argu. n° %d: %s - longueur %d\n", i, argv[i], longueur);
    }
    return 0;
}

/* ======= TP 04, Exercice 7======*/
/* Recodez les fonctions sur les str et chronometrer */
#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 1000
#define N2 2000


int mystrlen(char * chaine)
{
    int compteur=0;
    if (chaine!=NULL)
    {
        int i=0;
        char c=chaine[0];
        while (c!=0)
        {
            compteur++;
            i=i+1;
            c=chaine[i];
        }
        
    }
    return compteur;
}

int mystrcmp(char * chaine1, char * chaine2)
{
    int i=0;
    char c1=chaine1[0];
    char c2=chaine2[0];
    while ((c1==c2) && (c1!=0))
    {
        i=i+1;
        c1=chaine1[i];
        c2=chaine2[i];
    }
    return (c1-c2);
}

int mystrncmp(char * chaine1, char * chaine2, int n)
{
    int i=0;
    char c1=chaine1[0];
    char c2=chaine2[0];
    while ((c1==c2) && (c1!=0) && (i<n-1))
    {
        i=i+1;
        c1=chaine1[i];
        c2=chaine2[i];
    }
    return (c1-c2);
}

void mystrcpy(char * chaine1, char * chaine2)
{
    int i=0;
    char c;
    do
    {
        c=chaine2[i];
        chaine1[i]=c;
        i=i+1;
    } while (c!=0);
}

void mystrncpy(char * chaine1, char * chaine2, int n)
{
    int i=0;
    char c;
    do
    {
        c=chaine2[i];
        chaine1[i]=c;
        i=i+1;
    } while (c!=0 && i<n);
}

void mystrcat(char * chaine1, char * chaine2)
{
    int i=0;
    char c=chaine1[0];
    while (c!=0)
    {
        i++;
        c=chaine1[i];
    }
    int decalage=i;
    i=0;
    do
    {
        c=chaine2[i];
        chaine1[decalage+i]=c;
        i=i+1;
    } while (c!=0);
}

void mystrncat(char * chaine1, char * chaine2, int n)
{
    int i=0;
    char c=chaine1[0];
    while (c!=0)
    {
        i++;
        c=chaine1[i];
    }
    int decalage=i;
    i=0;
    do
    {
        c=chaine2[i];
        chaine1[decalage+i]=c;
        i=i+1;
    } while (c!=0 && i<n);
    chaine1[decalage+i]=0;
}

int main()
{
    char phrase[N][N];
    int i,j, r;
    for (i=0; i<N; i++)
    {
        for(j=0; j<i-1; j++)
        {
            phrase[i][j]=((i+j)%26)+97;
        }
        phrase[i][j]=0;
    }

    float fin_temps;
    float debut_temps=((float) clock())/CLOCKS_PER_SEC;
    /* TEST DE MY STRLEN */
    /*
     int taille;
    for(r=0; r<10000; r++)
    {
        for (i=0; i<N; i++)
        {
            //taille=mystrlen(phrase[i]);
            //taille=strlen(phrase[i]);
        }
    }
     */
    /* Prend environ 13.4 secondes avec mystrlen contre 0.48 sec avec strlen. */
    
    /* TEST DE MY STRCMP */
    /*
    int comparaison;
    for(r=0; r<1000000; r++)
    {
        for (i=0; i<N-1; i++)
        {

                comparaison=mystrcmp(phrase[i], phrase[i-1]);
                //comparaison=strcmp(phrase[i], phrase[i-1]);
        }
    }
    */
    /* Prend environ 7.18 secondes avec mystrcmp contre 5.58 sec avec strcmp. */
    
    /* TEST DE MY STRCPY */
    /*
     for(r=0; r<100000; r++)
    {
        for (i=1; i<N; i++)
        {
            
            mystrcpy(phrase[i-1], phrase[i]);
            //strcpy(phrase[i-1], phrase[i]);
        }
    }*/
    /* Prend environ 276 secondes avec mystrcpy contre 8.94 sec avec strcpy. */
    
    /* TEST DE MY STRCAT */
    char phrase2[N2][N2];
    
    for (i=0; i<N2; i++)
    {
        for(j=0; j<i-1; j++)
        {
            phrase2[i][j]=((i+j)%26)+97;
        }
        phrase2[i][j]=0;
    }
    debut_temps=((float) clock())/CLOCKS_PER_SEC;
    
    
    for(r=0; r<200; r++)
    {
        for (i=1; i<N-405; i++)
        {
            
            //mystrcat(phrase2[i], "aa");
            strcat(phrase2[i], "aa");
        }
    }
    /* Prend environ  136 milli-secondes avec mystrcat contre  7 milli-sec avec strcat. */
    
    fin_temps=((float) clock())/CLOCKS_PER_SEC;
    
    printf("Temps: %f\n", fin_temps-debut_temps);
    
    
    return 0;
}


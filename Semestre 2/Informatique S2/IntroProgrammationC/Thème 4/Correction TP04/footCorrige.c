#include <stdio.h>
#include <stdlib.h>
#define N 5 // nombre d'equipes dans la Ligue

void afficheMenu(char * prenom)
{
    printf("%s, appuyez sur n'importe quelle touche pour lancer le menu\n", prenom);
    
    /* Les deux lignes ci-dessous permettent d'attendre avant de relancer le menu.
     Les enlever si le fonctionnement est défaillant (cela dépend de la machine)*/
    char inutile;
    scanf("%c%c", &inutile, &inutile);
    
    printf("1. Afficher le tableau des resultats\n");
    printf("2. Modifier un resultat\n");
    printf("3. Calculer le nombre de points d'une equipe\n");
    printf("4. Afficher les scores\n");
    printf("5. Savoir quelle équipe a le plus de points\n");
    printf("6. Savoir quelle équipe a le plus de victoires à domicile\n");
    printf("7. Savoir quelle équipe a le plus de victoires à l'extérieur\n");
    printf("0. Quitter\n");
}

void afficheTab(int tab[N][N])
{
    int i,j;
    for (i=0; i<N; i++)
    {
        for (j=0; j<N; j++)
        {
            printf("%d ", tab[i][j]);
        }
        printf("\n");
    }
}

void modifCase(int tab[N][N])
{
    int equipeDomicile, equipeExterieur, nvCase;
    printf("Quel est le numéro de l'équipe jouant à domicile? ");
    scanf("%d", &equipeDomicile);
    printf("Quel est le numéro de l'équipe jouant à l'extérieur? ");
    scanf("%d", &equipeExterieur);
    printf("Quel est le nouveau resultat? \n (0: match nul, 1: equipe domicile gagnante, 2: equipe exterieur gagnante)\n");
    scanf("%d", &nvCase);
    tab[equipeDomicile][equipeExterieur]=nvCase;
}

int nbPoints(int resultat[N][N], int numEquipe)
{
    int i,j;
    int points=0;
    for (j=0; j<N; j++) // j parcourt les indices des colonnes du tableau: matchs à domicile de numEquipe
    {
        if (j!=numEquipe) // si on n'est as sur la diagonale
        {
            if (resultat[numEquipe][j]==0) // mathc nul
            {
                points=points+1;
            }
            else if (resultat[numEquipe][j]==1) // numEquipe a joué à domicile et a gagné
            {
                    points=points+3;
            }
        }
    }
    for (i=0; i<N; i++) // i parcourt maintenant les indices deslignes du tableau: matchs à l'extérieur de numEquipe
    {
        if (i!=numEquipe)// si on n'est pas sur la diagonale
        {
            if (resultat[i][numEquipe]==0) // match nul
            {
                points=points+1;
            }
            else if (resultat[i][numEquipe]==2) // numEquipe a joué à l'extérieur et a gagné
            {
                points=points+3;
            }
        }
    }
    return points;
}

void afficheScores(int tab[N][N])
{
    int i;
    printf("Recapitulatif des scores : ");
    for (i=0; i<N; i++)
    {
        printf("%d ", nbPoints(tab, i));
    }
    printf("\n");
}

int equipeLeader(int resultat[N][N])
{
    int maxPoints=0;
    int equipeMax;
    int points;
    int i;
    for (i=0; i<N; i++) // i va parcourir tous les numéros d'équipe
    {
        points=nbPoints(resultat, i);
        if (points>maxPoints)
        {
            maxPoints=points;
            equipeMax=i;
        }
    }
    return equipeMax;
}

int nbVictoires(int tab[N][N], int numEquipe, char domOuExt)
{
    int nb=0;
    if (domOuExt=='d')
    {
        int j;
        for (j=0; j<N; j++) // j parcourt les indices des colonnes du tableau: matchs à domicile de numEquipe
        {
            if ((j!=numEquipe) && tab[numEquipe][j]==1)
            {
                nb++;
            }
        }
    }
    else if (domOuExt=='e')
    {
        int i;
        for (i=0; i<N; i++) // i parcourt maintenant les indices deslignes du tableau: matchs à l'extérieur de numEquipe
        {
            if ((i!=numEquipe) && tab[i][numEquipe]==2)
            {
                nb++;
            }
        }
    }
    return nb;
}

int meilleureEquipeVictoires(int tab[N][N], char domOuExt)
{
    int maxVictoires=-1;
    int equipeMax=-1;
    int numEquipe;
    int nb;
    for (numEquipe=0; numEquipe<N; numEquipe++)
    {
        nb=nbVictoires(tab, numEquipe, domOuExt);
        if (nb>maxVictoires)
        {
            maxVictoires=nb;
            equipeMax=numEquipe;
        }
    }
    return equipeMax;
}

int meilleureEquipeDomicile(int tab[N][N])
{
    return meilleureEquipeVictoires(tab, 'd');
}

int meilleureEquipeExterieur(int tab[N][N])
{
    return meilleureEquipeVictoires(tab, 'e');
}

int main()
{
    //int resultat[N][N]={{0}};
    int resultat[N][N]={{0, 1, 1, 2, 0},
                        {2, 0, 1, 1, 1},
                        {1, 1, 0, 2, 2},
                        {2, 1, 1, 0, 0},
        {0, 0, 1, 2, 0}};
    /* Remarque: si N>5, alors l'initialisation ci-dessous remplit les cinq premieres colonnes des cinq premieres lignes, et toutes les autres cases sont mises à zéro.*/
    
    int choix;
    char prenom[10];
    printf("Bienvenue, quel est votre nom ? ");
    scanf("%s", prenom);
    int numEquipe;
    do
    {
        afficheMenu(prenom);
        printf("Votre choix: ");
        scanf("%d", &choix);
        switch(choix)
        {
            case 1:
                afficheTab(resultat);
                break;
            case 2:
                modifCase(resultat);
                break;
            case 3:
                printf("Choisissez un numero d'équipe: ");
                scanf("%d", &numEquipe);
                printf("Nb de points: %d\n", nbPoints(resultat, numEquipe));
                break;
            case 4:
                afficheScores(resultat);
                break;
            case 5:
                numEquipe=equipeLeader(resultat);
                printf("L'équipe ayant le plus de points est l'équipe %d avec %d points\n", numEquipe, nbPoints(resultat, numEquipe));
                break;
            case 6:
                numEquipe=meilleureEquipeDomicile(resultat);
                printf("L'équipe %d avec %d victoires à domicile est la meilleure dans ces conditions\n", numEquipe, nbVictoires(resultat, numEquipe, 'd'));
                break;
            case 7:
                numEquipe=meilleureEquipeExterieur(resultat);
                printf("L'équipe %d avec %d victoires à l'extérieur est la meilleure dans ces conditions\n", numEquipe, nbVictoires(resultat, numEquipe, 'e'));
                break;
        }
    } while (choix!=0);
    printf("Au revoir\n");
    
    return 0;
}

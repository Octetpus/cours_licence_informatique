#include<stdio.h>
#include<stdlib.h>
#define N 5


void wait_enter()
{
    char inutile;
    printf("(Press Enter to continue)\n");
    inutile=getchar();
    inutile=getchar();
}

int choixMenu(){
    int reponse;
    wait_enter();
    printf("======================================\n");
    printf("1. Changer le prix d'un produit.\n");
    printf("2. Connaitre le prix maximum.\n");
    printf("3. Connaître le prix total de tous les produits.\n");
    printf("4. Afficher le tableau des prix\n");
    printf("0. Quitter\n");
    printf("======================================\n");
    printf("Votre choix ? ");
    scanf("%d", &reponse);
    return reponse;
}


void initPrix(float tab[], int taille)
{
    float prix;
    int i;
    prix=0;
    for (i=0; i<taille; i++)
    {
        printf("Prix du produit numéro %d ? ", i);
        scanf("%f", &prix);
        tab[i]=prix;
    }
    printf("Merci\n");
}

void modifPrix(float tab[], int taille)
{
    int numeroProduit=-1;
    float nouveauPrix=0;
    printf("Numero du produit dont vous voulez changer le prix? ");
    scanf("%d", &numeroProduit);
    if (numeroProduit<0 || numeroProduit>=taille)
    {
        printf("Numero invalide\n");
    }
    else
    {
        printf("Quel est le nouveau prix? ");
        scanf("%f", &nouveauPrix);
        tab[numeroProduit]=nouveauPrix;
        printf("Opération effectuée:\n");
        printf("Nouveau prix du produit numero %d : %f\n", numeroProduit, nouveauPrix);
    }
}

void afficheTab(float tab[], int taille)
{
    int i;
    for (i=0; i<taille; i++)
    {
        printf("Numero %d : %f \n", i, tab[i]);
    }
}

float maxTab(float tab[], int taille)
{
    float max=0;
    int i;
    for (i=0; i<taille; i++)
    {
        if (tab[i]>max)
        {
            max=tab[i];
        }
    }
    return max;
}

float sommeTab(float tab[], int taille)
{
    float somme=0;
    int i;
    for (i=0; i<taille; i++)
    {
        somme=somme+tab[i];
    }
    return somme;
}

int main(){
    
    float tabPrix[N];
    afficheTab(tabPrix, N);
    float prixAAfficher=0;
    printf("=== Logiciel de gestion d'un stock de 5 produits==\n");
    initPrix(tabPrix, N);
    int choix=0;
    do {
        choix=choixMenu();
        switch (choix)
        {
            case 1:
                modifPrix(tabPrix, N);
                break;
            case 2:
                prixAAfficher=maxTab(tabPrix, N);
                printf("Prix max: %f \n", prixAAfficher);
                break;
            case 3:
                prixAAfficher=sommeTab(tabPrix, N);
                printf("Somme totale: %f \n", prixAAfficher);
                break;
            case 4:
                afficheTab(tabPrix, N);
                break;
            case 0:
                printf("Au revoir\n");
                break;
            default:
                printf("Mauvais choix\n");
        }
    } while (choix!=0);
    
    printf("Fin du programme\n");
    return 0;
}

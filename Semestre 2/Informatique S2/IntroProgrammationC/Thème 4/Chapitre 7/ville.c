#include <stdio.h>
#include <string.h>

int main()
{
    char lieu[50];
    char ville[]="Lyon";
    int n=strlen(ville);
    printf("%s (%d)\n", ville, n);
    char pays[20];
    printf("Dans quel pays?\n");
    scanf("%s", pays);
    char vraiPays[]="France";
    if (strcmp(pays, vraiPays))
    {
        sprintf(lieu, "%s en %s", ville, vraiPays);
    }
    else
    {
        sprintf(lieu, "%s, %s", ville, pays);
    }
    printf("%s\n", lieu);
    
    return 0;
    
    
}

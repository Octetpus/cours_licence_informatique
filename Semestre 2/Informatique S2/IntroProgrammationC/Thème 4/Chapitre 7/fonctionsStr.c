#include <stdio.h>
#include <string.h>

/* Attention: ne décommentez qu'une seule partie à la fois (toutes les autres parties doivent être commentées.
 Sinon, il va y avoir des conflits pour déclaration multiples de certaines variables */
int main()
{
    /* Test de sprintf */
    
     char prenom[]="Olivia";
     int date=1999;
     char phrase[100];
     sprintf(phrase, "%s né(e) en %d.\n", prenom, date);
     printf("%s", phrase); // Affiche: Olivia né(e) en 1999.
     
    
    
    /* Test snprintf */
    /*
     char prenom[]="Olivia";
     int date=1999;
     char phrase[9];
     snprintf(phrase,9,"%s né(e) en %d\n",prenom,date); // copie au plus 8 caract. normaux et rajoute '\0'
     printf("%s\n", phrase); //Affiche: Olivia n
     */
    
    /* Test de strcpy*/
    /*
     char prenom1[]="Lucas";
     char prenom2[20];
     strcpy(prenom2, prenom1);
     printf("%s - %s\n", prenom1, prenom2); // Affiche: Lucas - Lucas
     strcpy(prenom2,"Max");
     printf("%s - %s\n", prenom1, prenom2); // Affiche: Lucas - Max
     */
    
    
    
    /* Test strncpy */
    /*
     char prenom1[]="Lucas";
     char prenom2[4];
     strncpy(prenom2, prenom1, 4); // Copie 'L' 'u' 'c' 'a' et ne rajoute pas automatiquement un '\0'
     //prenom2[3]='\0'; // au cas où il n'ait pas été ajouté
     printf("%s - %s\n", prenom1, prenom2); //Peut planter car mauvaise detection de
     // la fin de chaine prenom2 car ne contient pas de '\0' final
     prenom2[3]='\0'; // Rectification: Mise du '\0' en dernière case de prenom2
     printf("%s - %s\n", prenom1, prenom2); // Bon affichage: Lucas - Luc
     */
    
    /* Test de strcat */
    /*
     char prenom[20];
     char nom[20];
     scanf("%s", prenom); // Imaginons que l'on tape Lucas
     scanf("%s", nom); // Imaginons que l'on tape Durand
     strcat(nom," "); //ajoute un espace à la fin de nom
     strcat(nom, prenom); //ajoute le contenu de prenom à la fin de nom
     printf("Nom complet: %s\n", nom); // Affiche: Nom complet: Durand Lucas
     */
    
    /* Test de strncat */
    /*
     char prenom[20];
     char nom[20];
     scanf("%s", prenom); // Imaginons que l'on tape Lucas
     scanf("%s", nom); // Imaginons que l'on tape Durand. nom contient donc 'D' 'u' 'r' 'a' 'n' 'd' '\0' (puis d'autres cases)
     strcat(nom," "); //ajoute un espace à la fin de nom. nom contient donc 'D' 'u' 'r' 'a' 'n' 'd' ' ' '\0'
     strncat(nom, prenom, 3); //ajoute le contenu des 3 premières cases (au plus) de prenom à la fin de nom, et ajoute '\0'
     // nom contient 'D' 'u' 'r' 'a' 'n' 'd' ' ' 'L' 'u' 'c' '\0'
     printf("Nom complet: %s\n", nom); // Affiche: Nom complet: Durand Luc
     
     char prenom2[6]="Luc"; // 4 cases sont utilisées: 'L' 'u' 'c' '\0'
     printf("%s\n", prenom2);
     strncat(prenom2, "astro", 2); // 6 cases sont utilisées: 'L' 'u' 'c' 'a' 's' '\0'  -> plus d'autres cases disponibles
     printf("%s\n", prenom2);
     
     char prenom3[6]="Luc"; // 4 cases sont utilisées: 'L' 'u' 'c' '\0'
     strncat(prenom3, "astro", 3); // Il n'y a pas 4+3 cases dans prenom3! --> Erreur ou écriture dans une mauvaise case
     printf("%s\n", prenom2);
     */
    
    /* Test de strcmp */
    /*
    int n;
    char nom1[]="Alice";
    char nom2[]="Baptiste";
    char nom3[]="Zoe";
    char nom4[]="Alban";
    n=strcmp(nom1, nom2); // n vaut -1
    n=strcmp(nom2, nom1); // n vaut 1
    n=strcmp(nom1, nom3); // n vaut -25
    n=strcmp(nom1, nom4); // n vaut 7 (diff entre i et b)
    n=strcmp(nom1, "Alice"); // n vaut 0
    n=strcmp(nom1, "alice"); // n vaut -32
    n=strcmp(nom3, "Zoey"); // n vaut -121
    printf("n vaut: %d\n", n);
     */
    
    /* Test de strncmp */
    /*
    int n;
    char nom1[]="Alice";
    char nom4[]="Alban";
    n=strncmp(nom1, nom4, 2); // n vaut 0
    n=strncmp(nom1, nom4, 4); // n vaut 7
    printf("n vaut %d\n",n);
     */
    
    
    
    
    return 0;
    
}

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    int n1, n2;
    if (argc>1)
    {
        n1=atoi(argv[1]);
        n2=atoi(argv[2]);
        printf("Somme: %d\n", n1+n2);
    }
    return 0;
}


#include <stdio.h>
#include <string.h>
#define M 10
#define N 7
#define C 5


/*====================== Corrigé Fiche TD04 ===================*/

/* Exercice 1*/

/* Question a */
/* Contenu de tab avant les boucles for
0  0  0 -9  2  0  0
0  0  0  5  0  0  0
0 18  0  0  0 10  0
0  0  0  0  0  0  0
0  0  0  0  0  0  0
 */

/* Contenu de tab apres les boucles for
 Chaque case de la dernière colonne reçoit la somme des cases de sa ligne
0  0  0 -9  2  0 -7
0  0  0  5  0  0  5
0 18  0  0  0 10 28
0  0  0  0  0  0  0
0  0  0  0  0  0  0
 */

/* Si on ne remet pas somme à 0 à chaque changemnt de ligne,
 on obtient:
 0  0  0 -9  2  0 -7
 0  0  0  5  0  0 -2
 0 18  0  0  0 10 26
 0  0  0  0  0  0 26
 0  0  0  0  0  0 26
Les cases de la dernière colonne reçoivent la somme cumulée de toutes les cases précédentes.
 */

/* Question b*/
/* L'affichage est: chou pomme (long. 10) */


/* Exercice 2 */

/* Fonction auxiliaire d'affichage */
void afficheTab2D(int tab[][M], int n)
{
    int i, j;
    int entree;
    for (i=0; i<n; i++)
    {
        for (j=0; j<M; j++)
        {
            entree=tab[i][j];
            if ((entree >= 0) && (entree<10))
            {
                printf(" "); // pour ameliorer l'affichage
            }
            printf("%d ", entree);
        }
        printf("\n");
    }
    
}

int sommeTab2D(int tab[][M], int n)
{
    int somme=0;
    int i,j;
    for (i=0; i<n; i++)
    {
        for (j=0; j<M; j++)
        {
            somme=somme+tab[i][j];
        }
    }
    return somme;
}

/* Exercice 3*/

void afficheFloatTab2D(float tab[][5], int n)
{
    int i, j;
    float entree;
    for (i=0; i<n; i++)
    {
        for (j=0; j<5; j++)
        {
            entree=tab[i][j];
            printf("%f ", entree);
        }
        printf("\n");
    }
    
}

/* Question a */
void plusGrandDesDeux(float t1[][5], float t2[][5], float t3[][5], int n)
{
    int i,j;
    float max;
    for (i=0; i<n; i++)
    {
        for (j=0; j<5; j++)
        {
            if (t1[i][j]>t2[i][j])
            {
                max=t1[i][j];
            }
            else
            {
                max=t2[i][j];
            }
            t3[i][j]=max;
        }
    }
}

/* Question b */
void sommeDesDeux(float t1[][5], float t2[][5], float t3[][5], int n)
{
    int i,j;
    for (i=0; i<n; i++)
    {
        for (j=0; j<5; j++)
        {
            t3[i][j]=t1[i][j]+t2[i][j];
        }
    }
}

/* Exercice 4*/
void initTab2D(int tab[][100], int n)
{
    int i,j;
    for (i=0; i<n; i++)
    {
        for (j=0; j<100; j++)
        {
            tab[i][j]=100*i+j;
        }
    }
}

/* Exercice 5 */
int chercheFloat2D(float tab[][C], int n, float cible)
{
    int i,j;
    for (i=0; i<n; i++)
    {
        for (j=0; j<C; j++)
        {
            if (tab[i][j]==cible)
            {
                printf("Cible %f trouvée en (%d, %d)\n", cible, i, j);
                return 1;
            }
        }
    }
    return 0;
}

/* Exercice 6 */
int nbEtudiants(char tab[][20], int nbLignes, char debut, char fin)
{
    int compteur=0;
    char premiereLettre;
    int i;
    for (i=0; i<nbLignes; i++)
    {
        premiereLettre=tab[i][0];
        if ((premiereLettre>= debut) && (premiereLettre<= fin))
        {
            compteur++;
        }
    }
    return compteur;
}

/* Exercice 7*/

int mystrlen(char * chaine)
{
    int compteur=0;
    if (chaine!=NULL)
    {
        int i=0;
        char c=chaine[0];
        while (c!=0)
        {
            compteur++;
            i=i+1;
            c=chaine[i];
        }
        
    }
    return compteur;
}

int mystrcmp(char * chaine1, char * chaine2)
{
    int i=0;
    char c1=chaine1[0];
    char c2=chaine2[0];
    while ((c1==c2) && (c1!=0))
    {
        i=i+1;
        c1=chaine1[i];
        c2=chaine2[i];
    }
    return (c1-c2);
}

int mystrncmp(char * chaine1, char * chaine2, int n)
{
    int i=0;
    char c1=chaine1[0];
    char c2=chaine2[0];
    while ((c1==c2) && (c1!=0) && (i<n-1))
    {
        i=i+1;
        c1=chaine1[i];
        c2=chaine2[i];
    }
    return (c1-c2);
}

void mystrcpy(char * chaine1, char * chaine2)
{
    int i=0;
    char c;
    do
    {
        c=chaine2[i];
        chaine1[i]=c;
        i=i+1;
    } while (c!=0);
}

void mystrncpy(char * chaine1, char * chaine2, int n)
{
    int i=0;
    char c;
    do
    {
        c=chaine2[i];
        chaine1[i]=c;
        i=i+1;
    } while (c!=0 && i<n);
}

void mystrcat(char * chaine1, char * chaine2)
{
    int i=0;
    char c=chaine1[0];
    while (c!=0)
    {
        i++;
        c=chaine1[i];
    }
    int decalage=i;
    i=0;
    do
    {
        c=chaine2[i];
        chaine1[decalage+i]=c;
        i=i+1;
    } while (c!=0);
}

void mystrncat(char * chaine1, char * chaine2, int n)
{
    int i=0;
    char c=chaine1[0];
    while (c!=0)
    {
        i++;
        c=chaine1[i];
    }
    int decalage=i;
    i=0;
    do
    {
        c=chaine2[i];
        chaine1[decalage+i]=c;
        i=i+1;
    } while (c!=0 && i<n);
    chaine1[decalage+i]=0;
}


int main()
{
	
    int tab[N][M]={{1,2,3}, {10, 45, 23}};
    tab[6][2]=9;
    tab[4][M-1]=16;
    afficheTab2D(tab, N);
    printf("\n");
    int somme=sommeTab2D(tab, N);
    printf("La somme est %d.\n", somme);
    
    
    float t1[N][5]={{1}, {-3.6}, {-7.82, 9.4}};
    float t2[N][5]={{-4.6}, {-15.6}, {0}, {-7.82, 9.4}};
    float t3[N][5];
    afficheFloatTab2D(t1, N);
    printf("\n");
    afficheFloatTab2D(t2, N);
    printf("\n");
    plusGrandDesDeux(t1, t2, t3, N);
    afficheFloatTab2D(t3, N);
    printf("\n");
    sommeDesDeux(t1, t2, t3, N);
    afficheFloatTab2D(t3, N);
    printf("\n");
    
    int t4[20][100];
    initTab2D(t4, 20);
    //afficheTab2D(t4, 20);
    printf("(7, 35): %d - (15, 99): %d\n", t4[7][35], t4[15][99]);
    
    int trouve=chercheFloat2D(t3,N, 9.4);
    printf("trouve: %d\n", trouve);
    
    char LInfo[5][20]={"DURAND", "BORIS", "VIAL", "HUGO", "FARGET"};
    int compteur=nbEtudiants(LInfo, 5, 'B', 'G');
    printf("Entre B et G: %d\n", compteur);
    
    char phrase[]="Salut toi!";
    //char phrase[]="";
    printf("strlen(phrase): %d, mystrlen(phrase):%d\n", (int) strlen(phrase), (int) mystrlen(phrase));
    
    
    char phrase1[]="Ervabla";
    char phrase2[]="Ervtruc";
    int n=3;
    printf("strcmp(phrase1, phrase2) %d, mystrcomp(phrase1, phrase2) %d, mystrcmp(phrase2, phrase1) %d\n", strcmp(phrase1, phrase2), mystrcmp(phrase1, phrase2), mystrcmp(phrase2, phrase1));
    printf("strncmp(phrase1, phrase2, ) %d, mystrncomp(phrase1, phrase2,  ) %d, mystrncmp(phrase2, phrase1, ) %d\n", strncmp(phrase1, phrase2, n), mystrncmp(phrase1, phrase2, n), mystrncmp(phrase2, phrase1, n));
    
    char phrase3[]="Aazertyuiopqssdfghjklm";
    char phrase4[]="Aazertyuiopqssdfghjklm";

    //strcpy(phrase3, phrase1);
    //mystrcpy(phrase4, phrase1);
    strncpy(phrase3, phrase1, 4);
    mystrncpy(phrase4, phrase1, 4);
    printf("phrase 1: %s -- phrase3: %s -- phrase4: %s\n", phrase1, phrase3, phrase4);
    
    char phrase5[100]="Hello hi";
    char phrase6[100]="Hello hi";
    //strcat(phrase5, phrase1);
    //mystrcat(phrase6, phrase1);
    strncat(phrase5, phrase1, 4);
    mystrncat(phrase6, phrase1, 4);
    printf("phrase 1: %s -- phrase5: %s -- phrase6: %s\n", phrase1, phrase5, phrase6);
    
    return 0;
}

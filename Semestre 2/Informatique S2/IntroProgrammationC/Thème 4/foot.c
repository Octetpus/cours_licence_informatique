#include <stdio.h>
#include <stdlib.h>
#define N 5

void afficheMenu(char * prenom)
{
    printf("%s, appuyez sur n'importe quelle touche pour lancer le menu\n", prenom);
    
    /* Les deux lignes ci-dessous permettent d'attendre avant de relancer le menu.
     Les enlever si le fonctionnement est défaillant (cela dépend de la machine)*/
    char inutile;
    scanf("%c%c", &inutile, &inutile);

    printf("1. Afficher le tableau des resultats\n");
    printf("2. Modifier un resultat\n");
    printf("3. Calculer le nombre de points d'une equipe\n");
    printf("4. Afficher les scores\n");
    printf("5. Savoir quelle équipe a le plus de points\n");
    printf("6. Savoir quelle équipe a le plus de victoires à domicile\n");
    printf("7. Savoir quelle équipe a le plus de victoires à l'extérieur\n");
    printf("0. Quitter\n");
}

/* pour modifCase

    
    printf("Quel est le numéro de l'équipe jouant à domicile? ");
    printf("Quel est le numéro de l'équipe jouant à l'extérieur? ");
    printf("Quel est le nouveau resultat? \n (0: match nul, 1: equipe domicile gagnante, 2: equipe exterieur gagnante)\n");
    */



int main()
{
    
    int resultat[N][N]={{0, 1, 1, 2, 0},
                        {2, 0, 1, 1, 1},
                        {1, 1, 0, 2, 2},
                        {2, 1, 1, 0, 0},
                        {0, 0, 1, 2, 0}};
    
    
    printf("Bienvenue, quel est votre nom ? ");
    // A COMPLETER pour recuperer le nom
    afficheMenu("Inconnu");
    printf("Votre choix: ");
    /*
    printf("Choisissez un numero d'équipe: ");
    
    printf("L'équipe ayant le plus de points est l'équipe %d avec %d points\n", A COMPLETER);
    
    printf("L'équipe %d avec %d victoires à domicile est la meilleure dans ces conditions\n", A COMPLETER);
    
    printf("L'équipe %d avec %d victoires à l'exterieur est la meilleure dans ces conditions\n", A COMPLETER );
    */
    printf("Au revoir\n");
    
    return 0;
}

#include <stdio.h>
#include <stdlib.h>

/* ============== Correction TP05 =========== */
/* == Exercice 2: premiere utilisation de malloc ====*/

void afficheTab(int * tab, int taille)
{
    int i;
    for (i=0; i<taille; i++)
    {
        printf("%d ", tab[i]);
    }
    printf("\n");
}

void initialiseDeuxEnDeux(int * tab, int taille, int debut)
{
    int i;
    int contenu=debut;
    for (i=0; i<taille; i++)
    {
        tab[i]=contenu;
        contenu=contenu+2;
    }
}

int main()
{
    
    int n;
    int debut;
    printf("Combien de cases dans le tableau?");
    scanf("%d", &n);
    
    int * tab;
    tab=(int *) malloc(n*sizeof(int));
    
    printf("Debuter à combien?");
    scanf("%d", &debut);
    initialiseDeuxEnDeux(tab, n, debut);
    afficheTab(tab, n);

    
    
    return 0;
}

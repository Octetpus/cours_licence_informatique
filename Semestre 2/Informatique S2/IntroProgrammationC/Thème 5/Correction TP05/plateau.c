#include <stdio.h>
#include <stdlib.h>

/* ============== Correction TP05 =========== */
/* == Exercice 4: plateau de jeu et coup ====*/

struct coorCoup
{
    int joueur;
    float xDeb;
    float xFin;
    float yDeb;
    float yFin;
};

typedef struct coorCoup coup;

struct plateauJeu
{
    coup * tab;
    int max;
    int prochaineCase;
};

typedef struct plateauJeu jeu;

void initPlateauJeu(jeu * j, int tailleDebut)
{
    j->tab=((coup *) malloc(tailleDebut*sizeof(coup)));
    j->max=tailleDebut;
    j->prochaineCase=0;
}

void copieCoup(coup * destination, coup source)
{
    destination->xDeb=source.xDeb;
    destination->yDeb=source.yDeb;
    destination->xFin=source.xFin;
    destination->yFin=source.yFin;
    destination->joueur=source.joueur;
}

void doubleTaille(jeu * pSurJeu)
{
    int n=pSurJeu->max;
    coup * gdTab=(coup *) malloc(2*n*sizeof(coup));
    int i;
    for (i=0; i<n; i++)
    {
        copieCoup(&gdTab[i], pSurJeu->tab[i]);
    }
    pSurJeu->max=2*n;
    free(pSurJeu->tab);
    pSurJeu->tab=gdTab;
}

void ajouteCoup(coup c, jeu * pSurJeu)
{
    if ((pSurJeu->prochaineCase)==(pSurJeu->max))
    {
        doubleTaille(pSurJeu);
    }
    coup * pSurProchain=&(pSurJeu->tab[pSurJeu->prochaineCase]);
    copieCoup(pSurProchain, c);
    (pSurJeu->prochaineCase)++;
}

void afficheCoup(coup c)
{
    printf("Joueur %d: Debut: (%f, %f) -> Fin (%f, %f)\n", c.joueur,c.xDeb, c.yDeb, c.xFin, c.yFin);
}

void afficheJeu(jeu * pSurJeu)
{
    int i;
    printf("Jeu:\n");
    for (i=0; i<pSurJeu->prochaineCase; i++)
    {
        afficheCoup(pSurJeu->tab[i]);
    }
    printf("Taille max du jeu actuel: %d (%d case(s) libre(s))\n", pSurJeu->max, (pSurJeu->max)-(pSurJeu->prochaineCase));
}


int main()
{
    jeu plateau;
    int tailleDebut=2;
    initPlateauJeu(&plateau, tailleDebut);
    
    coup c={1,0,0, 1.5, 3.5};
    ajouteCoup(c , &plateau);
    coup c2={2, 1,2, -1.5, -3.5};
    ajouteCoup(c2, &plateau);
    
    afficheJeu(&plateau);
    
    coup c3={1, -3.5, 6, 6, 7.5};
    ajouteCoup(c3, &plateau);
    afficheJeu(&plateau);
    
    free(plateau.tab);
    
    
    
    return 0;
}

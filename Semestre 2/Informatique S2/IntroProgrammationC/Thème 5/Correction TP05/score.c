#include <stdio.h>
#include <stdlib.h>

/* ============== Correction TP05 =========== */
/* == Exercice 1: score ====*/

struct score
{
    int butEquDom;
    int butEquExt;
};



void afficheScore(struct score sco)
{
    printf("Equipe a Domicile: %d - %d Equipe Exterieur\n", sco.butEquDom, sco.butEquExt);
}

void ajouteBut(struct score * pScore, char equipe)
{
   if (equipe=='d')
   {
       pScore->butEquDom++;
   }
   else if (equipe=='e')
   {
        pScore->butEquExt++;
   }
}


int main()
{
    struct score s={3,5};
    afficheScore(s);
    
    s.butEquDom=10;
    afficheScore(s);
    
    ajouteBut(&s, 'e');
    afficheScore(s);
    
    
    
    
    
    return 0;
}

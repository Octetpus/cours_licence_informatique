#include <stdio.h>
#include <stdlib.h>

/* ============== Correction TP05 =========== */
/* == Exercice 3: produit Inventaire ====*/

struct produitInventaire
{
    
    int quantite;
    int reference;
};

typedef struct produitInventaire produit;

void afficheProd(produit prod)
{
    printf("Produit réference : %d, quantité: %d\n", prod.reference, prod.quantite);
}

void ajoute(produit * prod, int quantiteAAjouter)
{
    prod->quantite+=quantiteAAjouter;
}


void initialise(produit * tab, int taille)
{
    int i;
    int ref;
    int qte;
    for (i=0; i<taille; i++)
    {
        printf("Produit référence n°?");
        scanf("%d", &ref);
        printf("Quantité?");
        scanf("%d", &qte);
        tab[i].quantite=qte;
        tab[i].reference=ref;
    }
}

void afficheTabProd(produit * tab, int taille)
{
    int i;
    printf("===Tableau de produits:===\n");
    for (i=0; i<taille; i++)
    {
        afficheProd(tab[i]);
    }
    printf("==========================\n");
}

void ajouteProd(produit * tab, int taille, produit nv)
{
    int refCherchee=nv.reference;
    int i;
    for (i=0; i<taille; i++)
    {
        if (tab[i].reference==refCherchee)
        {
            tab[i].quantite+=nv.quantite;
        }
    }
}

int main()
{
    produit prod={40, 106};
    afficheProd(prod);
    
    int n;
    printf("Combien de produits dans l'inventaire?");
    scanf("%d", &n);
    
    produit * tab=(produit *) malloc(n*sizeof(produit));
    initialise(tab, n);
    afficheTabProd(tab, n);
    
    ajouteProd(tab, n, prod);
    afficheTabProd(tab, n);
    
    
    return 0;
}

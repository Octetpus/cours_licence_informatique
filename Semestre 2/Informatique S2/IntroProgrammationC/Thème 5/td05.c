#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

/* =================== CORRECTION TD05 ========*/

/* Exercice 1*/
struct st_personne
{
	char prenom[20];
	float poids;
	float taille;
	
};

typedef struct st_personne personne;

void affichePersonne(personne p)
{
	printf("Prenom: %s - poids: %f - taille %f \n", p.prenom, p.poids, p.taille);
}


float imc(personne p)
{
	return (p.poids)/(p.taille * p.taille);
}

void plusGrand(personne p1, personne p2)
{
	if (p1.taille>=p2.taille)
	{
		printf("%s (%f m) est plus grand que %s (%f m)\n", p1.prenom, p1.taille, p2.prenom, p2.taille);
	}
	else
	{
		printf("%s (%f m) est plus grand que %s (%f m)\n", p2.prenom, p2.taille, p1.prenom, p1.taille);
	}

}

void maigri(personne* pointPers, float kilosPerdus)
{
	pointPers-> poids = pointPers->poids - kilosPerdus;

}


/* Exercice 2 */


struct ingredient{
    char nom[10];
    float prix;
    int nbCalories;
};

void afficheIng(struct ingredient ing)
{
    printf("Ingredient| nom: %s, prix %f, calories: %d\n", ing.nom, ing.prix, ing.nbCalories);
}

struct pizza{
    int typePate;
    int base;
    int nbIng;
    struct ingredient * tabIng;
};

void affichePizza(struct pizza pizz)
{
    printf("Pizza pate %d, base %d, nb ing %d\n", pizz.typePate, pizz.base, pizz.nbIng);
    int i;
    for (i=0; i<pizz.nbIng; i++)
    {
        afficheIng(pizz.tabIng[i]);
    }
    printf("\n");
}

struct ingredient nouvelIngredient()
{
    struct ingredient ing;
    printf("Nom? (max 10 car.)");
    scanf("%s", ing.nom);
    printf("Prix?");
    scanf("%f", &ing.prix);
    printf("Nb calories?");
    scanf("%d", &ing.nbCalories);
    return ing;
}

void remplirPizza(struct pizza * psurPizz, int nbIng)
{
    printf("Type pate?");
    scanf("%d", &psurPizz->typePate);
    printf("Base?");
    scanf("%d", &psurPizz->base);
    psurPizz->nbIng=nbIng;
    int repet=0;
    int i;
    int somme=0;
    struct ingredient ing;
    while (somme<psurPizz->nbIng)
    {
        printf("Nouvel ingredient (%d restants)\n", nbIng-somme);
        ing=nouvelIngredient();
        printf("Combien de fois cet ingredient?");
        scanf("%d", &repet);
        for(i=somme; i<somme+repet; i++)
        {
            psurPizz->tabIng[i]=ing;
        }
        somme+=repet;
    }
}

void remplirPizzaDyn(struct pizza * psurPizz)
{
    printf("Type pate?");
    scanf("%d", &psurPizz->typePate);
    printf("Base?");
    scanf("%d", &psurPizz->base);
    printf("nb Ing?");
    scanf("%d", &psurPizz->nbIng);
    psurPizz->tabIng= (struct ingredient *) malloc((psurPizz->nbIng)*sizeof(struct ingredient));
    int repet=0;
    int i;
    int somme=0;
    struct ingredient ing;
    while (somme<psurPizz->nbIng)
    {
        printf("Nouvel ingredient (%d restants)\n", (psurPizz->nbIng)-somme);
        ing=nouvelIngredient();
        printf("Combien de fois cet ingredient?");
        scanf("%d", &repet);
        for(i=somme; i<somme+repet; i++)
        {
            psurPizz->tabIng[i]=ing;
        }
        somme+=repet;
    }
}


int calories(struct pizza pizz)
{
    int i;
    int somme=0; // somme des calories
    for (i=0; i<pizz.nbIng; i++)
    {
        somme+=pizz.tabIng[i].nbCalories;
    }
    return somme;
}

float cout(struct pizza pizz)
{
    int i;
    float somme=0; // somme des prix
    for (i=0; i<pizz.nbIng; i++)
    {
        somme+=pizz.tabIng[i].prix;
    }
    return somme;
}

void nom(struct pizza pizz, char * destination, int tailleChaine)
{
    int i;
    strcpy(destination, "");
    int borne;
    if (tailleChaine>=pizz.nbIng*2+1)
    {
        borne=pizz.nbIng;
    }
    else
    {
        borne=(tailleChaine-1)/2;
    }
    for (i=0; i<borne; i++)
    {
        strncat(destination, pizz.tabIng[i].nom,2);
        
    }
    
}



/* Exercice 3*/


void modifInt(int * pointeurSurInt)
{
    printf("Nouvelle valeur voulue:");
    scanf("%d", pointeurSurInt);
}

void echangeInt(int * pointeurA, int * pointeurB)
{
    int temp=*pointeurA;
    *pointeurA=*pointeurB;
    *pointeurB=temp;
}

void modifPP(int ** pointeurDePointeur)
{
    **pointeurDePointeur=**pointeurDePointeur+1;
    // ou (**pointeurDePointeur)++;
    // Attention à ne pas oublier les parentheses!
    
}


/* Exercice 4*/

int funMin(int a, int b, int c)
{
    int min;
    if (a <= b)
    {
        if (c<=a)
        {
            min=c;
        }
        else
        {
            min=a;
        }
    }
    else if (b<=c)
    {
        min=b;
    }
    else
    {
        min =c;
    }
    return min;
}

int funMax(int a, int b, int c)
{
    int max;
    if (a >= b)
    {
        if (c>=a)
        {
            max=c;
        }
        else
        {
            max=a;
        }
    }
    else if (b>=c)
    {
        max=b;
    }
    else
    {
        max =c;
    }
    return max;
}


void minMax(int a, int b, int c, int * psurMin, int * psurMax)
{
    (*psurMin)=funMin(a,b,c);
    (*psurMax)=funMax(a,b,c);
}


int main()
{
    /* Exo 1 */
    personne pA={"Alain", 78.3, 1.80};
	personne pB={"Benedicte", 67.3, 1.65};
	
	affichePersonne(pA);
	affichePersonne(pB);
	
	float tmp;
	tmp=imc(pA);
	printf("IMC d'Alain: %f \n", tmp);
	
	plusGrand(pA, pB);
	
	maigri(&pA, 4.3);
	
	plusGrand(pB, pA);
	affichePersonne(pA);
    
    /* Exo 2*/
    
    
    struct pizza pizz;
    struct ingredient tab[5];
    pizz.tabIng=tab;
    remplirPizza(&pizz, 5);
    
    affichePizza(pizz);
    
    printf("Calories: %d, cout: %f\n", calories(pizz), cout(pizz));
    char buffer[12];
    nom(pizz, buffer, 12);
    printf("Nom pizza: %s\n", buffer);
    
    
    struct pizza pizzDyn;
    remplirPizzaDyn(&pizzDyn);
    affichePizza(pizzDyn);
     
    
    /* Exo 3*/
    
    int a,b,c;
    a=10;
    b=20;
    c=30;
    modifInt(&a);
    printf("a=%d\n", a);
    echangeInt(&a, &b);
    printf("a=%d b=%d\n", a, b);
    int * pointeursurC=&c;
    modifPP(&pointeursurC);
    printf("c=%d \n", c);
    
    /* Exo 4 */
    
    int min, max;
    minMax(40, 12, 54, &min, &max);
    printf("min: %d, max: %d\n", min, max);
    
    
    return 0;
}

#include <stdio.h>
#include <string.h>

int main()
{
    char phrase[100];
    char fruit[]="pomme";
    char legume[]="chou";
    if (strcmp(fruit, legume)<0)
    {
        strcpy(phrase, fruit);
        strcat(phrase, " ");
        strcat(phrase, legume);
    }
    else
    {
        strcpy(phrase, legume);
        strcat(phrase, " ");
        strcat(phrase, fruit);
    }
    int n=strlen(phrase);
    sprintf(phrase, "%s (long. %d)", phrase, n);
    printf("%s\n", phrase);
    
    return 0;
}

#include <stdio.h>

struct personne
{
    char prenom[20];
    char nom[20];
    int age;
};

typedef struct personne pers;

int main()
{
    pers client={"Alice", "Durand", 24};
    client.age=20;
    printf("Nouveau nom de famille?");
    scanf("%s", client.nom);
    printf("Votre client est: %s %s, agé(e) de %d ans\n", client.prenom, client.nom, client.age);
    
    struct personne * p=&client;
    p->age=100;
    printf("Nv age: %d\n", p->age);
    
    (*p).age=50;
    printf("Nv age: %d\n", (*p).age);
    return 0;
}


#include <stdio.h>

void initialise(int tab[], int n)
{
    int i;
    for (i=0; i<n; i++)
    {
        tab[i]=i;
    }
}

void initialiseBis(int * tab, int n)
{
    int i;
    for (i=0; i<n; i++)
    {
        tab[i]=i;
    }
}

int main()
{
    int t1[5];
    initialise(t1, 5);
    
    int t2[10];
    initialiseBis(t2, 10);
    
    printf("t1[3] contient %d\n", t1[3]);
    printf("t2[3] contient %d\n", t2[3]);
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void affiche(int tab[], int n)
{
    int i;
    for (i=0; i<n; i++)
    {
        printf("%d ", tab[i]);
    }
    printf("\n");
}

void initRandom(int tab[], int n)
{
    int i;
    for (i=0; i<n; i++)
    {
        tab[i]=(rand()% 100); // entier aleatoire entre 0 et 99 (car modulo 100)
    }
}

int main(int argc, char * argv[])
{
    srand(time(NULL));
    if (argc>1)
    {
        int n;
        n=atoi(argv[1]);
        int * tab= (int *) malloc(n*sizeof(int));
        initRandom(tab, n);
        affiche(tab,n);
        printf("Test: %p - %p - %p - %p\n", tab, &tab[0], &tab[1], &tab);
        
        free(tab);
        tab=NULL;
    }
    else
    {
        printf("Cet executable doit recevoir un parametre\n");
    }
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Exemple d'un jeu de cartes à plusieurs joueurs, ou chaque joueur commence avec une réserve d'argent aléatoire */

/* Version 3 (la plus complexe):
 Comme la version 2:
 - La structure jeuCartes (et son alias jeu) contient le nombre de cartes, le nombre de joueurs,
 et le tableau d'argent.
 - La fonction qui crée le jeu doit donc faire appel à TabDebutJeu pour réserver la mémoire
 et initialiser le tableau d'argent.
 - Le tableau d'argent est déclaré dynamiquement (puis initialisé) dans la fonction TabDebutJeu

 
 Contrairement à la version 2:
 - La variable de type jeu (ou strcut jeuCartes) ne sera jamais recopiée, même lorsqu'elle sera utilisée
   comme argument ou valeur de retour d'une fonction.
     * Comme valeur de retour: On allouera dynamiquement sa mémoire dans la fonction creeJeuParPointeurs
        et on renverra le pointeur, dont on se servira systématiquement dans la suite.
        Regarder la fonction creeJeuParPointeur.
     * Comme argument: la signatures de la fonction afficheCaracteristique a été modifiée pour prendre l'argument
       de type jeu par adresse pour éviter les recopiages inutiles. Cette décision est indépendante des
       changements effectués dans la fonction de création du jeu
     * Remarquez l'utilisation du symbole point pour accéder aux champs d'une variable de type jeu,
       ou flèche pour accéder aux champs d'un jeu pointé par un pointeur.
 - on a remis les fonctions dans le meme ordre que dans jeuVersion1.c, donc on a besoin de déclarer les
   prototypes des struct et fonctions en début de programme.
  */

struct jeuCartes;
 
typedef struct jeuCartes jeu; // créé un alias jeu, raccourci de struct jeuCartes
 
jeu * creeJeuParPointeur(int);
void afficheCaracteristiques(jeu *);
void afficheArgentJoueurs(float *, int);
float * TabDebutJeu(int);
int estNumeroCorrect(int, int);
void transfert(float *, int, int, int, float);



struct jeuCartes
{
    int nbCartes;
    int nbJoueurs;
    float * tabArgent;
};

/* inutile maintenant car déja dit plus haut */
// typedef struct jeuCartes jeu; // créé un alias jeu, raccourci de struct jeuCartes


jeu * creeJeuParPointeur(int n)
{
    /* doit creer un nouveau jeu avec 32 cartes, n joueurs, et un tableau d'argent initialisé
     aléatoirement */
    
    /* déclaration d'un pointeur sur un jeu, avec allocation dynamique */
    jeu * pointeurSurNvJeu= (jeu *) malloc(sizeof(jeu));
    
    /* Initialisation des champs */
    pointeurSurNvJeu->nbCartes= 32;
    pointeurSurNvJeu->nbJoueurs= n;
    pointeurSurNvJeu->tabArgent=TabDebutJeu(n);
    
    /* Ci-dessous: on renvoie directement le pointeur, car la mémoire allouée pour le jeu n'est pas libérée
      automatiquement. Il n'y aura donc aucune recopie, la valeur de retoru sera l'adresse où il faut aller lire
      le contenu de la structure */
    /* On peut afficher l'adresse du jeu dans la fonction, puis plus tard dans le main, pour voir que les
     deux adresses sont les memes */
    printf("Adresse du jeu dans creeJeuParPointeur: %p\n", pointeurSurNvJeu);
    return pointeurSurNvJeu;
}

void afficheCaracteristiques(jeu * pointeurSurJeu)
{
    printf("Ce jeu se déroule avec %d cartes et %d joueurs.\n", pointeurSurJeu->nbCartes, pointeurSurJeu->nbJoueurs);
    afficheArgentJoueurs(pointeurSurJeu->tabArgent, pointeurSurJeu->nbJoueurs);
}


void afficheArgentJoueurs(float * argent, int nbJoueurs)
{
    printf("========== Etat actuel des finances des joueurs ===========\n");
    int i;
    for (i=0; i<nbJoueurs; i++)
    {
        printf("Joueur n°%d : %f dollars\n", i, argent[i]);
    }
    printf("==========================================================\n");
}

float * TabDebutJeu(int nbJoueurs)
{
    /* doit créer un tableau de float contenant une case par joueur (=argent du joueur),
     puis l'initialiser à un nombre aléatoire entre 0 et 10 (entier, au début du jeu) */
    float * tab = (float *) malloc(nbJoueurs*sizeof(float));
    int i;
    for (i=0; i<nbJoueurs; i++)
    {
        tab[i]= rand() % 11; // nombre aléatoire entre 0 et 10
    }
    return tab;
}

int estNumeroCorrect(int numeroJoueur, int nbJoueurs)
{
    if ((numeroJoueur >=0) && (numeroJoueur<nbJoueurs))
    {
        return 1; // numero valide, renvoie vrai
    }
    else
    {
        return 0; // numero invalide, renvoie faux
    }
    // Rq: on aurait pu seulement écrire return ((numeroJoueur >=0) && (numeroJoueur<nbJoueurs));
}

void transfert(float * argent, int nbJoueurs, int joueurADebiter, int joueurACrediter, float montantACrediter)
{
    if (estNumeroCorrect(joueurADebiter, nbJoueurs) && estNumeroCorrect(joueurACrediter, nbJoueurs))
    {
        argent[joueurACrediter]=argent[joueurACrediter]+montantACrediter;
        argent[joueurADebiter]=argent[joueurADebiter]-montantACrediter;
        /* Rq: les deux lignes ci-dessus ont le même comportement que les deux lignes ci-dessous:
         argent[joueurACrediter]+= montantACrediter;
         argent[joueurACrediter]-= montantACrediter;
         */
         
    }
    else
    {
        printf("Operation impossible, au moins un numero de joueur invalide\n");
    }
}



int main()
{
    srand(time(NULL)); // initialisation de l'aléatoire
    int n; // n désignera le nombre de joueurs
    printf("Combien de joueurs?");
    scanf("%d", &n);
    
    jeu * pointeurSurJeu = creeJeuParPointeur(n); /* cree un jeu, l'initalise, puis renvoie un pointeur
                                                   pour que l'on puisse y accéder directement, sans recopie */
    printf("Adresse du jeu dans le main: %p\n", pointeurSurJeu); // on peut vérifier que l'adresse est la meme
    
    afficheCaracteristiques(pointeurSurJeu);
    
    
    /* Si le joueur 0 doit faire un transfert de 3.5 dollars au joueur 1 */
    transfert(pointeurSurJeu->tabArgent, n, 0, 1, 3.5);
    afficheCaracteristiques(pointeurSurJeu);
    
    /* Imaginer d'autres instructions ... */
    
    free(pointeurSurJeu->tabArgent); // on libère la mémoire allouée dynamiquement au tableau d'argent
    free(pointeurSurJeu); // on libère la mémoire allouée au jeu

    return 0;
}


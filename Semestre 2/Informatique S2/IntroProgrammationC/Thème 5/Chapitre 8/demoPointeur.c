#include <stdio.h>

int main(){
    
    
    float f = 123.45;
    float * p; // p est un pointeur sur un float
    p=NULL;
    p = &f; // p pointe sur la case de la variable f. /* Donc *p désigne désormais le contenu de f */
    printf("La valeur est %f\n", *p);
    f = 67.89;
    printf("Ah, c’est devenu %f\n", *p);
    *p = 10.11;
    printf("Ca vient de rechanger… La valeur est maintenant %f…\n", f);
    
    return 0;
}

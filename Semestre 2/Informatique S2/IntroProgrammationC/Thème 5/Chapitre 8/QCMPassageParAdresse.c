#include <stdio.h>

struct groupePersonnes
{
    int numeroGroupe;
    int nbPersonnes;
};

typedef struct groupePersonnes groupe;

void afficheGroupe(groupe g)
{
    printf("Numero du groupe: %d - ", g.numeroGroupe);
    printf("nb de pers.: %d\n", g.nbPersonnes);
}

void changeValeurs1(groupe * pointeurSurGroupe, int nvNum, int nvNbPers)
{
    pointeurSurGroupe->numeroGroupe=nvNum;
    pointeurSurGroupe->nbPersonnes=nvNbPers;
}

void changeValeurs2(groupe g, int nvNum, int nvNbPers)
{
    g.numeroGroupe=nvNum;
    g.nbPersonnes=nvNbPers;
}



int main()
{
    groupe gr;
    changeValeurs1(&gr,  3, 12);
    afficheGroupe(gr);
    changeValeurs2(gr, 1, 35);
    afficheGroupe(gr);
    
    return 0;
}

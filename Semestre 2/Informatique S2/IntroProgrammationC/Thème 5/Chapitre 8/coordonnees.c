#include <stdio.h>
#include <stdlib.h>

struct coordonnees
{
    float abscisse;
    float ordonnee;
};
typedef struct coordonnees coord;

void remiseAZero(coord * pointeur)
{
    pointeur->abscisse=0;
    pointeur->ordonnee=0;
}

coord creeCoord ()
{
    coord B;
    B.abscisse=0;
    B.ordonnee=0;
    return B;
}

coord * creeCoordp ()
{
    coord * p= (coord *) malloc(sizeof(coord));
    p->abscisse=0;
    p->ordonnee=0;
    return p;
}



int main()
{
    coord A={3.4, 47};
    coord * pointeurSurA;
    pointeurSurA=&A;

    printf("A: %f %f\n", A.abscisse, A.ordonnee);
    
    pointeurSurA->abscisse=9.9;
    //pointeurSurA.abscisse=5.67;
    (*pointeurSurA).ordonnee= -3.2;
    
    remiseAZero(pointeurSurA);
    printf("A: %f %f\n", A.abscisse, A.ordonnee);
    
    coord C;
    C=creeCoord();
    C.abscisse=4;
    C.ordonnee=-3.89;
    printf("C: %f %f\n", C.abscisse, C.ordonnee);
    remiseAZero(&C);
    printf("C: %f %f\n", C.abscisse, C.ordonnee);
    
    coord * pSurD; // pointeur sur un coord
    pSurD=creeCoordp();
    pSurD->abscisse=48.02;
    pSurD->ordonnee=-52.8;
    printf("D: %f %f\n", pSurD->abscisse, pSurD->ordonnee);
    remiseAZero(pSurD);
    printf("D: %f %f\n", pSurD->abscisse, pSurD->ordonnee);

    
    return 0;
}


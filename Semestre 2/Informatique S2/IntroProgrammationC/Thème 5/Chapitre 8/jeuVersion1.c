#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Exemple d'un jeu de cartes à plusieurs joueurs, ou chaque joueur commence avec une réserve d'argent aléatoire */

/* Version 1 (la plus simple):
 La structure jeuCartes (et son alias jeu) contient seulement le nombre de cartes et le nombre de joueurs.
 Les réserves d'argent sont stockées à part, dans un tableau de float appelé argent dans le programme principal.
 Ce tableau est déclaré dynamiquement (puis initialisé) dans la fonction TabDebutJeu.
 Les variables de type jeu (ou strcut jeuCartes) seront passées par copie
 lorsqu'elles sont utilisées comme argument ou valeur de retour d'une fonction */

struct jeuCartes
{
    int nbCartes;
    int nbJoueurs;
};

typedef struct jeuCartes jeu; // créé un alias jeu, raccourci de struct jeuCartes

jeu creeJeu(int n)
{
    jeu nvJeu; // déclaration d'une variable de type jeu
    
    /* Initialisation des champs */
    nvJeu.nbCartes= 32;
    nvJeu.nbJoueurs= n;
    
    /* Ci-dessous: nvJeu est passé par copie (possible pour une structure, contrairement aux tableaux)
     (chaque champ sera recopié dans la variable qui recevra la valeur de retour de type jeu) */
    return nvJeu;
}

void afficheCaracteristiques(jeu j)
{
    printf("Ce jeu se déroule avec %d cartes et %d joueurs.\n", j.nbCartes, j.nbJoueurs);
}

void afficheArgentJoueurs(float * argent, int nbJoueurs)
{
    printf("========== Etat actuel des finances des joueurs ===========\n");
    int i;
    for (i=0; i<nbJoueurs; i++)
    {
        printf("Joueur n°%d : %f dollars\n", i, argent[i]);
    }
    printf("==========================================================\n");
}

float * TabDebutJeu(int nbJoueurs)
{
    /* doit créer un tableau de float contenant une case par joueur (=argent du joueur),
    puis l'initialiser à un nombre aléatoire entre 0 et 10 (entier, au début du jeu) */
    float * tab = (float *) malloc(nbJoueurs*sizeof(float));
    int i;
    for (i=0; i<nbJoueurs; i++)
    {
        tab[i]= rand() % 11; // nombre aléatoire entre 0 et 10
    }
    return tab;
}

int estNumeroCorrect(int numeroJoueur, int nbJoueurs)
{
    if ((numeroJoueur >=0) && (numeroJoueur<nbJoueurs))
    {
        return 1; // numero valide, renvoie vrai
    }
    else
    {
        return 0; // numero invalide, renvoie faux
    }
    // Rq: on aurait pu seulement écrire return ((numeroJoueur >=0) && (numeroJoueur<nbJoueurs));
}

void transfert(float * argent, int nbJoueurs, int joueurADebiter, int joueurACrediter, float montantACrediter)
{
    if (estNumeroCorrect(joueurADebiter, nbJoueurs) && estNumeroCorrect(joueurACrediter, nbJoueurs))
    {
        argent[joueurACrediter]=argent[joueurACrediter]+montantACrediter;
        argent[joueurADebiter]=argent[joueurADebiter]-montantACrediter;
        /* Rq: les deux lignes ci-dessus ont le même comportement que les deux lignes ci-dessous:
         argent[joueurACrediter]+= montantACrediter;
         argent[joueurACrediter]-= montantACrediter;
         */
         
    }
    else
    {
        printf("Operation impossible, au moins un numero de joueur invalide\n");
    }
}



int main()
{
    srand(time(NULL)); // initialisation de l'aléatoire
    int n; // n désignera le nombre de joueurs
    printf("Combien de joueurs?");
    scanf("%d", &n);
    
    jeu j = creeJeu(n); // cree un jeu, l'initialise, et recopie le résultat dans j
    
    afficheCaracteristiques(j);
    
    float * argent; // Futur tableau qui compte l'argent de chaque joueur
    argent=TabDebutJeu(n); // Creation et initialisation du tableau d'argent
    
    afficheArgentJoueurs(argent, n);
    
    /* Si le joueur 0 doit faire un transfert de 3.5 dollars au joueur 1 */
    transfert(argent, n, 0, 1, 3.5);
    
    afficheArgentJoueurs(argent, n);
    
    /* Imaginer d'autres instructions ... */
    
    free(argent); // libère la mémoire allouée dynamiquement pour argent

    return 0;
}


#include <stdio.h>
# define M 7

void afficheTab2D(int tab[][M], int n)
{
    int i, j;
    int entree;
    for (i=0; i<n; i++)
    {
        for (j=0; j<M; j++)
        {
            entree=tab[i][j];
            if ((entree >= 0) && (entree<10))
            {
                printf(" "); // pour ameliorer l'affichage
            }
            printf("%d ", entree);
        }
        printf("\n");
    }
    
}

int main()
{
    int tab[5][M]={{0}};
    tab[1][3]=5;
    tab[2][1]=18;
    tab[2][5]=10;
    tab[0][3]=-9;
    tab[0][4]=2;
    afficheTab2D(tab, 5);
    printf("\n");
    int i,j, somme;
    //somme=0; // pour la derniere partie de la question
    for (i=0; i<5; i++)
    {
        somme=0;
        for (j=0; j<M-1; j++)
        {
            somme=somme+tab[i][j];
        }
        tab[i][M-1]=somme;
    }
    afficheTab2D(tab, 5);
    return 0;
}


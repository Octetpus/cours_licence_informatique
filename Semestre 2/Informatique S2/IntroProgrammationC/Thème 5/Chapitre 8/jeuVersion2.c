#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Exemple d'un jeu de cartes à plusieurs joueurs, ou chaque joueur commence avec une réserve d'argent aléatoire */

/* Version 2 (difficulté intermédiaire):
 Contrairement à la version 1:
 - La structure jeuCartes (et son alias jeu) contient maintenant le nombre de cartes, le nombre de joueurs,
   et le tableau d'argent.
 - La fonction qui crée le jeu doit donc faire appel à TabDebutJeu pour réserver la mémoire
   et initialiser le tableau d'argent.
 
 Comme la version 1:
 - Le tableau d'argent est déclaré dynamiquement (puis initialisé) dans la fonction TabDebutJeu
   (même fonction que précédemment, maais c'est l'endroit où la fonction est appelée qui a été changé).
 - Les variables de type jeu (ou strcut jeuCartes) seront passées par copie
   lorsqu'elles sont utilisées comme argument ou valeur de retour d'une fonction */

/* Remarque:
 Si l'on avait voulu garder les fonctions écrites dans le meme ordre que dans jeuVersion1.c,
 il aurait fallu déclarer le prototype de la struct et de toutes les fonctions en début de fichier.
 Une fois que le compilateur sait qu'elles existent, les codes sources peuvent etre dans
 n'importe quel ordre.*/
/*
struct jeuCartes;
 
typedef struct jeuCartes jeu;
 
jeu creeJeuParCopie(int);
void afficheCaracteristiques(jeu);
void afficheArgentJoueurs(float *, int);
float * TabDebutJeu(int);
int estNumeroCorrect(int, int);
void transfert(float *, int, int, int, float);
 */


struct jeuCartes
{
    int nbCartes;
    int nbJoueurs;
    float * tabArgent;
};

typedef struct jeuCartes jeu; // créé un alias jeu, raccourci de struct jeuCartes


float * TabDebutJeu(int nbJoueurs)
{
    /* doit créer un tableau de float contenant une case par joueur (=argent du joueur),
     puis l'initialiser à un nombre aléatoire entre 0 et 10 (entier, au début du jeu) */
    float * tab = (float *) malloc(nbJoueurs*sizeof(float));
    int i;
    for (i=0; i<nbJoueurs; i++)
    {
        tab[i]= rand() % 11; // nombre aléatoire entre 0 et 10
    }
    return tab;
}


jeu creeJeuParCopie(int n)
{
    /* doit creer un nouveau jeu avec 32 cartes, n joueurs, et un tableau d'argent initialisé
     aléatoirement */
    jeu nvJeu; // déclaration d'une variable de type jeu
    
    /* Initialisation des champs */
    nvJeu.nbCartes= 32;
    nvJeu.nbJoueurs= n;
    nvJeu.tabArgent=TabDebutJeu(n);
    

    
    /* Ci-dessous: nvJeu est passé par copie (possible pour une structure, contrairement aux tableaux)
    (chaque champ sera recopié dans la variable qui recevra la valeur de retour de type jeu) */
    /* On peut afficher l'adresse du jeu dans la fonction, puis plus tard dans le main, pour voir que les
     deux adresses sont différentes */
    printf("Adresse du jeu dans la fonction creeJeuParCopie: %p\n", &nvJeu);
    return nvJeu;
    
    
}



void afficheArgentJoueurs(float * argent, int nbJoueurs)
{
    printf("========== Etat actuel des finances des joueurs ===========\n");
    int i;
    for (i=0; i<nbJoueurs; i++)
    {
        printf("Joueur n°%d : %f dollars\n", i, argent[i]);
    }
    printf("==========================================================\n");
}

void afficheCaracteristiques(jeu j)
{
    printf("Ce jeu se déroule avec %d cartes et %d joueurs.\n", j.nbCartes, j.nbJoueurs);
    afficheArgentJoueurs(j.tabArgent, j.nbJoueurs);
}


int estNumeroCorrect(int numeroJoueur, int nbJoueurs)
{
    if ((numeroJoueur >=0) && (numeroJoueur<nbJoueurs))
    {
        return 1; // numero valide, renvoie vrai
    }
    else
    {
        return 0; // numero invalide, renvoie faux
    }
    // Rq: on aurait pu seulement écrire return ((numeroJoueur >=0) && (numeroJoueur<nbJoueurs));
}

void transfert(float * argent, int nbJoueurs, int joueurADebiter, int joueurACrediter, float montantACrediter)
{
    if (estNumeroCorrect(joueurADebiter, nbJoueurs) && estNumeroCorrect(joueurACrediter, nbJoueurs))
    {
        argent[joueurACrediter]=argent[joueurACrediter]+montantACrediter;
        argent[joueurADebiter]=argent[joueurADebiter]-montantACrediter;
        /* Rq: les deux lignes ci-dessus ont le même comportement que les deux lignes ci-dessous:
         argent[joueurACrediter]+= montantACrediter;
         argent[joueurACrediter]-= montantACrediter;
         */
         
    }
    else
    {
        printf("Operation impossible, au moins un numero de joueur invalide\n");
    }
}



int main()
{
    srand(time(NULL)); // initialisation de l'aléatoire
    int n; // n désignera le nombre de joueurs
    printf("Combien de joueurs?");
    scanf("%d", &n);
    
    jeu j = creeJeuParCopie(n); // cree un jeu, l'initialise, et recopie le résultat dans j
    printf("Adresse de j: %p\n", &j);
    
    afficheCaracteristiques(j);
    
    
    /* Si le joueur 0 doit faire un transfert de 3.5 dollars au joueur 1 */
    transfert(j.tabArgent, n, 0, 1, 3.5);
    afficheCaracteristiques(j);
    
    /* Imaginer d'autres instructions ... */
    
    free(j.tabArgent); // libère la mémoire allouée dynamiquement pour le tableau d'argent

    return 0;
}


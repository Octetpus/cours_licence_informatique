#include <stdio.h>
#include <string.h>

struct etu
{
    char nom[20];
    char cursus[50];
    int numero;
    float notes[2];
};

typedef struct etu etudiant;

int main()
{
    etudiant e;
    sprintf(e.nom, "Marie");
    strcpy(e.cursus, "L1 Maths Info");
    e.numero=34;
    e.notes[0]=14.5;
    e.notes[1]=9.75;
    
    printf("Nom %s, Num %d\n", e.nom, e.numero);
    printf("Notes: info %f maths %f\n", e.notes[0], e.notes[1]);
    return 0;
}

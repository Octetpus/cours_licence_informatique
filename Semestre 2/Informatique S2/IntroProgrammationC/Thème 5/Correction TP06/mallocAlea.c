#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* ======= Correction TD 6 ============== */

/* Exercice 1 */

/* Non-nécéssaire pour le TD */

int indiceDuMax(int * tab, int n)
{
    int indiceMax=0;
    int i;
    for(i=0; i<n; i++)
    {
        if (tab[i]>tab[indiceMax])
        {
            indiceMax=i;
        }
    }
    return indiceMax;
}

void echangeDernierTab(int * tab, int n, int i)
{
    int temp=tab[i];
    tab[i]=tab[n-1];
    tab[n-1]=temp;
}

void tri(int * tab, int taille)
{
    int tailleFictive;
    for (tailleFictive=taille; tailleFictive>1; tailleFictive--)
    {
        int indiceMax=indiceDuMax(tab, tailleFictive);
        echangeDernierTab(tab, tailleFictive, indiceMax);
    }
}


/* Fin partie non demandée en TD */


void afficheTab(int * tab, int taille)
{
    int i;
    for (i=0; i<taille; i++)
    {
        printf("%d ", tab[i]);
    }
    printf("\n");
}

void initialiseRandom(int * tab, int taille, int borne)
{
    int i;
    int contenu;
    for (i=0; i<taille; i++)
    {
        contenu=rand()%(borne+1);
        tab[i]=contenu;
    }
}

int main()
{
    srand(time(NULL));
    
    int n;
    int borne;
    printf("Entier n (nombre de cases pour le tableau)?");
    scanf("%d", &n);
    
    int * tab;
    tab=(int *) malloc(n*sizeof(int));
    
    printf("Borne pour les entiers aléatoires dans le tableau?");
    scanf("%d", &borne);
    initialiseRandom(tab, n, borne);
    afficheTab(tab, n);
    
    tri(tab, n);
    afficheTab(tab, n);

    
    
    return 0;
}

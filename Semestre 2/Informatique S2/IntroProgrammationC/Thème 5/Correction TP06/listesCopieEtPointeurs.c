#include <stdio.h>
#include <stdlib.h>

/* == Complément: liste chainee sur les entiers avec passage par copie lorsque c'est possible == */
/* == Fiche TD06, exos 2 à 4 ====*/

struct maillonPrListe
{
    int valeur;
    struct maillonPrListe * suivant;
};

typedef struct maillonPrListe maillon;

struct listeChainee
{
    maillon * debut;
};

typedef struct listeChainee liste;

liste creeListeVide()
{
    liste * pointeurSurListe;
    pointeurSurListe=(liste *) malloc (1*sizeof(liste));
    pointeurSurListe->debut=NULL;
    return (*pointeurSurListe);
}

void afficheListe(liste l)
{
    maillon * pSurMaillon;
    pSurMaillon=l.debut;
    while (pSurMaillon!=NULL)
    {
        printf("%d ->", pSurMaillon->valeur);
        pSurMaillon=pSurMaillon->suivant;
    }
    printf("NULL\n");
}

void insereDebutListe(liste * pointeurSurListe, int valeurAAjouter)
{
    maillon * pSurMaillon;
    pSurMaillon=(maillon *) malloc(1*sizeof(maillon));
    pSurMaillon->valeur=valeurAAjouter;
    pSurMaillon->suivant=pointeurSurListe->debut;
    pointeurSurListe->debut=pSurMaillon;
}

void suppPremierListe(liste * pSurListe)
{
    maillon * pSurDeuxieme;
    pSurDeuxieme=pSurListe->debut->suivant;
    free(pSurListe->debut);
    pSurListe->debut=pSurDeuxieme;
}

int taille(liste li)
{
    int i=0;
    maillon * pSurMaillon=li->debut;
    while(pSurMaillon!=NULL)
    {
        i=i+1;
        pSurMaillon=pSurMaillon->suivant;
    }
    return i;
}

int lireValeurMaillon(liste li, int index)
{
    int i=0;
    maillon * pSurMaillon=li->debut;
    while((pSurMaillon!=NULL) && (i<index))
    {
        i=i+1;
        pSurMaillon=pSurMaillon->suivant;
    }
    if (pSurMaillon!=NULL)
    {
        return pSurMaillon->valeur;
    }
    else
    {
        printf("Liste pas assez longue, fin à l'indice %d, indice demandé: %d\n", i-1, index);
        return 0;
    }
}

void modifValeur(liste li, int index, int nvValeur)
{
    int i=0;
    maillon * pSurMaillon=pSurListe->debut;
    while((pSurMaillon!=NULL) && (i<index))
    {
        i=i+1;
        pSurMaillon=pSurMaillon->suivant;
    }
    if (pSurMaillon!=NULL)
    {
        pSurMaillon->valeur=nvValeur;
    }
    else
    {
        printf("Liste pas assez longue, fin à l'indice %d, indice demandé: %d\n", i-1, index);
    }
}


void libererListe(liste * pointeurSurListe)
{
    maillon * pSurMaillon;
    maillon * pSurPrecedent;
    pSurMaillon=pointeurSurListe->debut;
    while (pSurMaillon!=NULL)
    {
        pSurPrecedent=pSurMaillon;
        pSurMaillon=pSurMaillon->suivant;
        free(pSurPrecedent);
    }
}


int main()
{
    liste li=creeListeVide();
    insereDebutListe(&li, 13);
    insereDebutListe(&li, -5);
    insereDebutListe(&li, 6);
    afficheListe(li);
    
    suppPremierListe(&li);
    afficheListe(li);
    
    libererListe(&li);
    
    printf("Fin du programme\n");
    return 0;
}

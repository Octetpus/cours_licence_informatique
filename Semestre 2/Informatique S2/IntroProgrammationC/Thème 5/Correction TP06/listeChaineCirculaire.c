#include <stdio.h>
#include <stdlib.h>


/* ============== Correction liste chaine circulaire =========== */
/* == Fiche 06, exo 9 du TD et exo 3 du TP ====*/

struct maillonPrListe
{
    int valeur;
    struct maillonPrListe * suivant;
};

typedef struct maillonPrListe maillon;

struct listeCirculaire{
    maillon * debut;
};

typedef struct listeCirculaire listeCi;

listeCi * creeListeVide()
{
    listeCi * l=(listeCi *) malloc(sizeof(listeCi));
    l->debut=NULL;
    return l;
}


maillon * dernierMaillon(listeCi *l)
{
    maillon * premierMaillon=l->debut;
    if (premierMaillon!=NULL)
    {
        maillon * e=premierMaillon;
        while (e->suivant != premierMaillon)
        {
            e=e->suivant;
        }
        return e;
    }
    else
    {
        return NULL;
    }
}

void ajouteMaillonTete(listeCi * l, int valeur)
{
    maillon * e=(maillon *) malloc(sizeof(maillon));
    e->valeur=valeur;
    if (l->debut==NULL)
    {
        e->suivant=e;
    }
    else
    {
        e->suivant=l->debut;
        maillon * dernier=dernierMaillon(l);
        dernier->suivant=e;
    }
    l->debut=e;
}

void afficheListe(listeCi * l)
{
    maillon * e=l->debut;
    printf("debut ");
    
    if (e!=NULL)
    {
        do
        {
            printf("%d -> ", e->valeur);
            e=e->suivant;
        } while(e!=(l->debut));
    }
    printf("fin \n");
}


void ajouteMaillonFin(listeCi * l, int valeur)
{
    maillon * e=(maillon *) malloc(sizeof(maillon));
    e->valeur=valeur;
    if (l->debut==NULL)
    {
        l->debut=e;
        e->suivant=e;
    }
    else
    {
        maillon * dernier=dernierMaillon(l);
        e->suivant=l->debut;
        dernier->suivant=e;
    }
}

void supprimeMaillonTete(listeCi * l)
{
    maillon * premierMaillon=l->debut;
    if (premierMaillon!=NULL)
    {
        maillon * dernier=dernierMaillon(l);
        if (premierMaillon!=dernier)
        {
            dernier->suivant=premierMaillon->suivant;
            l->debut=premierMaillon->suivant;
        }
        else
        {
            l->debut=NULL;
        }
        free(premierMaillon);
    }
}

int suivant(listeCi * l)
{
    int v=l->debut->valeur;
    maillon * deuxieme=l->debut->suivant;
    l->debut=deuxieme;
    return v;
}

void destruction(listeCi * l)
{
    maillon * premierMaillon=l->debut;
    if (premierMaillon!=NULL)
    {
        maillon * e=premierMaillon->suivant;
        maillon * e2;
        while (e!=premierMaillon)
        {
            e2=e->suivant;
            free(e);
            e=e2;
        }
        free(premierMaillon);
    }
    free(l);
}



int main()
{
    listeCi * l=creeListeVide();
    ajouteMaillonTete(l, 3);
    ajouteMaillonTete(l, 5);
    ajouteMaillonTete(l, 12);
    ajouteMaillonTete(l, 200);
    
    
    afficheListe(l);
    
    
    ajouteMaillonFin(l, 120);
    ajouteMaillonFin(l, 300);
    afficheListe(l);
    supprimeMaillonTete(l);
    afficheListe(l);
    
    int i;
    for (i=0; i<10; i++)
    {
        printf("Suivant: %d\n", suivant(l));
        afficheListe(l);
    }
    
    destruction(l);
    return 0;
}


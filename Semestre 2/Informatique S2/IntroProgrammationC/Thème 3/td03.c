#include<stdio.h>
#define N 6

/* ========== Correction TD03 =========== */

/* Exercice 1 a */
void afficheTabInt(int tab[], int taille)
{
    int i;
    for (i=0; i<taille; i++)
    {
        printf("%d ", tab[i]);
    }
    printf("\n");
}

/* Fonction Auxiliaire pour mes tests */
void afficheTabFloat(float tab[], int taille)
{
    int i;
    for (i=0; i<taille; i++)
    {
        printf("%f ", tab[i]);
    }
    printf("\n");
}


/* Exercice 1 b */
void afficheAlEnvers(float tab[], int taille)
{
    int i;
    for (i=taille-1; i>=0; i--)
    {
        printf("%f ", tab[i]);
    }
    printf("\n");
}

/* Exercice 2 a*/
int somme(int tab[], int taille)
{
    int somme=0;
    int i;
    for (i=0; i<taille; i++)
    {
        somme=somme+tab[i];
    }
    return somme;
}

/* Exercice 2 b*/
int sommePairs(int tab[], int taille)
{
    int somme=0;
    int i;
    for (i=0; i<taille; i++)
    {
        if (tab[i]%2==0)
        {
            somme=somme+tab[i];
        }
    }
    return somme;
}

/* Exercice 2 c*/
int sommeNegatifs(int tab[], int taille)
{
    int somme=0;
    int i;
    for (i=0; i<taille; i++)
    {
        if (tab[i]<0)
        {
            somme=somme+tab[i];
        }
    }
    return somme;
}



/* Exercice 3 */
int plateau(int tab[], int taille)
{
    if (taille==0)
    {
        return 0;
    }
    else
    {
        int taillePlateauActuel=1;
        int maxPlateau=1;
        int precedent=tab[0];
        int i;
        for (i=1; i<taille; i++)
        {
            if (tab[i]==precedent) // je continue sur le meme plateau
            {
                taillePlateauActuel++;
                if (taillePlateauActuel>maxPlateau) // je mets a jour maxPlateau avec le plateau en cours, s'il est plus grand
                {
                    maxPlateau=taillePlateauActuel;
                }
            }
            else // je vais commencer un nouveau plateau
            {
                
                taillePlateauActuel=1; // je commence un nouveau plateau
                precedent=tab[i];
            }
        }
        return maxPlateau;
    }
}

/* Exercice 4 */
int estTrie(float tab[], int taille)
{
    int triOK=1;
    if (taille!=0)
    {
        float precedent=tab[0];
        int i=1;
        while (i<taille && triOK)
        {
            if (tab[i]<precedent)
            {
                triOK=0; // le tri n'est plus OK
            }
            precedent=tab[i];
            i++;
            
        }
    }
    return triOK; // retourne un booleen
}


/* Exercice 5 a*/
int recherche1(int tab[], int taille, int cible)
{
    int i=0;
    while (i<taille)
    {
        if (tab[i]==cible)
        {
            return i;
        }
        i++;
    }
    return -1;
}


/* Exercice 5 b */
int recherche2(int tab[], int taille, int cible)
{
    int curseurDebut=0;
    int curseurFin=taille-1;
    int indexMilieu;
    while (curseurFin>=curseurDebut)
    {
        indexMilieu=(curseurDebut+curseurFin)/2;
        if (tab[indexMilieu]==cible)
        {
            return indexMilieu;
        }
        else if (tab[indexMilieu]>cible)
        {
            curseurFin=indexMilieu-1;
        }
        else
        {
            curseurDebut=indexMilieu+1;
        }
    }
    return -1;
}


/* La fonction recherche2 est beaucoup plus rapide (à condition de savoir préalabement que le tableau est trié).
 On peut évoquer que le nombre de passages dans la boucle sera "environ" log(n) où n est la taille du tableau, contre n pour recherche1.
 Ne pas trop insister sur la complexité */

/* Exercice 6 */
void insereElement(float tab[], int taille, float nvElement, int position)
{
    if (position<taille && position >=0)
    {
        int i;
        for (i=taille-1; i>position; i--)
        {
            tab[i]=tab[i-1];
        }
        tab[position]=nvElement;
    }
    else
    {
        printf("Indice non valide: %d\n", position);
    }
}

/* Exercice 7 */
int supprimeElem(float tab[], int tailleUtilisee, int position)
{
    if (position<tailleUtilisee && position>=0)
    {
        int i;
        for (i=position; i<tailleUtilisee-1; i++)
        {
            tab[i]=tab[i+1];
        }
        return tailleUtilisee-1;
    }
    else
    {
        printf("Indice non valide: %d\n", position);
        return -1;
    }
}



/* Exercice 8*/
void concatenation(float t1[], int n1, float t2[], int n2, float t3[], int n3)
{
    if (n3>=n1+n2)
    {
        int i;
        for (i=0; i<n1; i++)
        {
            t3[i]=t1[i];
        }
        for (i=0; i<n2; i++)
        {
            t3[n1+i]=t2[i];
        }
    }
}

void unionTab(float t1[], int n1, float t2[], int n2, float t3[], int n3)
{
    if (n3>=n1+n2)
    {
        int i;
        int tailleRestante1=n1;
        int tailleRestante2=n2;
        for (i=0; i<n1+n2; i++)
        {
            if (tailleRestante1==0 || ((tailleRestante2>0) && t2[0]<t1[0]))
            {
                t3[i]=t2[0];
                tailleRestante2=supprimeElem(t2, tailleRestante2, 0);
            }
            else
            {
                t3[i]=t1[0];
                tailleRestante1=supprimeElem(t1, tailleRestante1, 0);
            }
            
        }
    }

}


int main()
{
    int t01[]={4, 5, -2, 52, -10, 8};
    printf("%d \n", somme(t01, N));
    printf("%d \n", sommePairs(t01, N));
    printf("%d \n", sommeNegatifs(t01, N));
    
    float t02[]={-4.5, 2.25, 67.45, -89.5, 10, 0};
    afficheAlEnvers(t02, N);
    
    int t03[]={8, 5, 5, 6, 6, 6, 6, 2, 2, 2};
    printf("Longueur plateau: %d\n", plateau(t03, 10));
    int t03bis[]={8, 5, 5, 6, 6, 6, 6, 2, 2, 2, 4, 4, 4, 4, 4, 4};
    printf("Longueur plateau: %d\n", plateau(t03bis, 16));
    
    int t03ter[]={8, 8, 8, 6, 5, 6, 6, 2, 2, 1, 4, 4};
    printf("Longueur plateau: %d\n", plateau(t03ter, 12));
    
    
    float t04[]={-5.6, -3.2, 0, 8.9, 15, 52.34};
    float t04bis[]={-5.6, 0, 2, 3.9, 1, 2.34};
    printf("Tableau trié? %d\n", estTrie(t04, N));
    printf("Tableau trié? %d\n", estTrie(t02, N));
    printf("Tableau trié? %d\n", estTrie(t04bis, N));
    /*
    printf("Recherche de 52 dans t01: %d\n", recherche1(t01, N, 52));
    printf("Recherche de 10 dans t01: %d\n", recherche1(t01, N, 10));
    
    int t06[]={-10, -2, 4, 5, 8, 52};
    printf("Recherche dicho de 52 dans t06: %d\n", recherche2(t06, N, 52));
    printf("Recherche dicho de 8 dans t06: %d\n", recherche2(t06, N, 8));
    printf("Recherche dicho de 10 dans t06: %d\n", recherche2(t06, N, 10));
    int t05[]={3, 36};
    printf("Recherche dicho de 3 dans t05: %d\n", recherche2(t05, 2, 3));
    printf("Recherche dicho de 36 dans t05: %d\n", recherche2(t05, 2, 36));
    printf("Recherche dicho de 5 dans t05: %d\n", recherche2(t05, 2, 5));
    
    float t01f[]={4, 5, -2, 52, -10, 8};
    supprimeElem(t01f, N, 2);
    afficheTabFloat(t01f, N);
    
    insereElement(t01f, N, -2, 2);
    afficheTabFloat(t01f, N);
    supprimeElem(t01f, N, 0);
    supprimeElem(t01f, N, N-2);
    afficheTabFloat(t01f, N);
    insereElement(t01f, N, 4, 0);
    insereElement(t01f, N, 8, N-1);
    afficheTabFloat(t01f, N);
    
    printf("\n\nExercice 9 \n");
    float t091a[]={-2, 3, 6, 12, 12, 56};
    float t091b[]={-2, 3, 6, 12, 12, 56};
    float t092a[]={-10, 0, 5};
    float t092b[]={-10, 0, 5};
    float t093[9];
    concatenation(t091a, 6, t092a, 3, t093, 9);
    afficheTabFloat(t093, 9);
    unionTab(t091b, 6, t092b, 3, t093, 9);
    afficheTabFloat(t093, 9);
     */
}

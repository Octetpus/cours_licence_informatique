#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 10
#define M 5
#define TAILLE 10

/* =============== Correction TP03 ============= */

/* Exercice 1: voir dans le main */

/* Exercice 2*/
void afficheTab(int tab[], int n)
{
    int i;
    for (i=0; i<n; i++) // i parcourt les indices des cases tableau: de 0 à n-1
    {
        printf("%d ", tab[i]);
    }
    printf("\n");
}

/* Exercice 3*/

void initClavier(int tab[], int n)
{
    int i;
    int element=-1;
    for (i=0; i<n; i++)
    {
        printf("Entier suivant (positif):" );
        scanf("%d", &element);
        /* Si l'élément donné n'est pas positif, on "bloque" (on répète la question) jusqu'a obtenir une valeur positive*/
        while (element<0)
        {
            printf("Incorrect, il faut que l'entier soit positif. Recommencez:");
            scanf("%d", &element);
        }
        tab[i]=element; // on remplit la case i du tableau avec le contenu de la variable element
    }
}

/* Exercice 4*/


void initRandom(int tab[], int n)
{
    int i;
    int element=-1;
    for (i=0; i<n; i++)
    {
        element=rand()%11; // element recoit un entier aleatoire entre 0 et 10 (appel à la fonction rand, qui ne prend pas de paramètre, puis modulo 11
        tab[i]=element;
    }
}

/* Exercice 5: changez les constantes N et M */

/* Exercice 6 */

int rechercheTab(int tab[], int n, int x)
{
    int i=0;
    while (i<n)
    {
        if (tab[i]==x) //si on a trouvé l'element qu'on cherchait dans la case i
        {
            return i; // on renvoie i (on quitte la fonction, le reste n'est pas executé
        }
        i++;
    }
    return -1; // si on arrive ici sans avoir déjà quitter la fonction, cela signifie que l'on n'a pas trouvé w dans le tableau donc on doit renvoyer -1
}

/* Exercice 7 a*/
int maxTab(int tab[], int taille)
{
    int max=tab[0]; // contiendra la plus grande valeur jusqu'ici
    /* Attention a un piege courant: on pourrait avoir envie d'initialiser max a 0,
     mais dans ce cas notre fonction ne renverra pas la bonne valeur si tous les elements du tableau sont strictement negatifs (on renverrait 0, qui n'est pas dans le tableau) */
    int i;
    for (i=0; i<taille; i++)
    {
        if (tab[i]>max)
        {
            max=tab[i];
        }
    }
    return max;
}

/* Exercice 7 a*/
int maxTabPairs(int tab[], int taille)
{
    int max;
    /* On ne sait pas comment initialiser max ici: 0 ne marche pas s'il peut y avoir des entiers negatifs
     dans le tableau; tab[0] ne marche pas s'il n'est pas pair ...*/
    int aTrouvePremierPair=0; /* On va donc utiliser un booléen aTrouvePremierPair qui vaudra Faux tant qu'on n'a pas encore trouvé un entier pair pour initialiser max */
    int i;
    for (i=0; i<taille; i++)
    {
        if (tab[i]%2==0) // si le contenu de la case i est pair
        {
            if (!aTrouvePremierPair) // si je n'ai pas encore trouvé d'entier pair avant
            {
                max=tab[i]; // j'initialise max
                aTrouvePremierPair=1; // désormais, j'ai trouvé un premier entier pair
            }
            else if (tab[i]>max)
                /* le else if m'assure que je n'évalue la condition que si la condtion du if était fausse, donc ici si j'ai déjà trouvé un entier pair. Donc j'ai déjà initialisé max, je peux comparer la valeur avec le contenu de la case du tableau */
            {
                max=tab[i];
            }
        }
    }
    if (aTrouvePremierPair)
    {
        return max;
    }
    else
    {
        return -1;
    }
}

// une autre version equivalente ci-dessous
int maxTabPairsv2(int tab[], int taille)
{
    int max=-1; // j'initialise max à une valeur "spéciale" -1
    /* Tant que max vaudra -1, cela signifiera que je n'ai pas encore trouvé d'entier pair dans mon entier */
    int i;
    for (i=0; i<taille; i++)
    {
        if (tab[i]%2==0)
        {
            if (max==-1) // max a encore sa valeur spéciale
            {
                max=tab[i]; // je viens de mettre dans max la valeur du premier entier pair du tableau
            }
            else if (tab[i]>max)
            {
                max=tab[i];
            }
        }
    }
    return max;
}

/* Exercice 8*/
int maxPosTab(int tab[], int taille)
{
    int max=maxTab(tab, taille);
    return rechercheTab(tab, taille, max);
}

int maxPosTab2(int tab[], int taille)
{
    int max=tab[0]; /* Maximum vu jusqu'ici */
    int posMax=0; /* Position du maximum vu jusqu'ici */
    int i;
    for (i=0; i<taille; i++)
    {
        if (tab[i]>max)
        {
            /* Si je passe ici, c'est que la case i contient un entier plus grand que le max vu jusqu'ici*/
            /* Donc je mets à jour max, et aussi posMax */
            max=tab[i];
            posMax=i;
        }
    }
    return posMax;
}

/* Exercice 9 */
void echangeDernierTab(int tab[], int taille, int position)
{
    if (position>=0 && (position <=taille-1)) // je verifie que la position est un indice valide du tableau
    {
        int temp=tab[position]; // je stocke dans une variable temporaire avant d'écraser la valeur de tab[position]
        tab[position]=tab[taille-1];
        tab[taille-1]=temp;
    }
    else
    {
        printf("Indice non valide: %d\n", position);
    }
}

/* Exercice 10*/
void triSelection(int tab[], int n)
{
    int tailleRestante; // taille "fictive" du tableau: au début on considérera qu'il a n cases, puis seulement n-1 cases, puis seulement  n-2, etc... jusqu'à une case.
    int posMax;
    for (tailleRestante=n; tailleRestante>0; tailleRestante--)
    {
        posMax=maxPosTab2(tab, tailleRestante);
        echangeDernierTab(tab, tailleRestante, posMax);
        /* attention ici à bien prendre tailleRestante et pas taille! */
    }
}

/* Exercice 11 */
void swap(int tab[], int i, int j)
/* echange le contenu de la case i avec celui de la case j */
{
    int temp=tab[i];
    tab[i]=tab[j];
    tab[j]=temp;
}

void triBullev1(int tab[], int taille)
{
    int i, j;
    for (i=0; i<taille; i++)
    {
        for (j=0; j<taille-1; j++)
        {
            if (tab[j]>tab[j+1])
            {
                swap(tab, j, j+1);
            }
        }
    }
}

void triBullev2(int tab[], int taille)
{
    int i, j;
    for (i=0; i<taille; i++)
    {
        for (j=0; j<taille-i-1; j++)
        {
            if (tab[j]>tab[j+1])
            {
                swap(tab, j, j+1);
            }
        }
    }
}

void triBullev3(int tab[], int taille)
{
    int i, j;
    i=0;
    int encoreUnTour=1; // booléen valant Vrai s'il est nécessaire de faire un tour de plus
    while (encoreUnTour && i<taille)
    {
        encoreUnTour=0; // début du tour numero i, je mets encoreUnTour à Faux
        for (j=0; j<taille-i-1; j++)
        {
            if (tab[j]>tab[j+1])
            {
                encoreUnTour=1; // j'ai fait au moins un échange dans ce tour, donc il me faudra encore un tour de plus
                swap(tab, j, j+1);
            }
        }
        i++;
    }
    /* Je sors de ma boucle while si i est arrivé à taille, ou bien si j'ai eu un tour entier sans faire aucun échange (les tours suivants ne feraient donc aucun échange eux non plus, donc ils sont inutiles */
    
    //printf("nb de tours: %d \n" , i);
}

int main()
{
    srand(time(NULL));
    
    /* Commentez/Décommentez des parties du main selon ce que vous souhaitez tester */
    
    /* Exercices 1 à 5*/
    int t1[N]={1};
    int t2[M]={1};
    afficheTab(t1, N);
    afficheTab(t2, M);

    int tab[]={50, 60, 12, 67, 32, -5, -15, 78, 2, -1};
    afficheTab(tab, TAILLE);
    
    //initClavier(t1, N);
    //initClavier(t2, M);
    afficheTab(t1, N);
    afficheTab(t2, M);
    
    initRandom(t1, N);
    initRandom(t2, M);
    afficheTab(t1, N);
    afficheTab(t2, M);
    
    /* Autres tests */
    printf("maxTabPairs sur tab: %d\n", maxTabPairs(tab, TAILLE));
    printf("maxTabPairsv2 sur tab: %d\n", maxTabPairs(tab, TAILLE));
    printf("maxPosTab sur tab: %d\n", maxPosTab(tab, TAILLE));
    printf("maxPosTab2 sur tab: %d\n", maxPosTab2(tab, TAILLE));
    //triSelection(tab, TAILLE);
    afficheTab(tab, TAILLE);
    
    triBullev1(tab, TAILLE);
    afficheTab(tab, TAILLE);
    int tab2[]={50, 60, 12, 67, 32, -5, -15, 78, 2, -1};
    triBullev2(tab2, TAILLE);
    afficheTab(tab2, TAILLE);
    
    int tab3[]={50, 60, 12, 67, 32, -5, -15, 78, 2, -1};
    triBullev3(tab3, TAILLE);
    afficheTab(tab3, TAILLE);
    
    int tab4[]={-50, -60, 12, 67, 132, 155, 145, 135};
    triBullev3(tab4, 8);
    afficheTab(tab4, 8);
    
    return 0;
}

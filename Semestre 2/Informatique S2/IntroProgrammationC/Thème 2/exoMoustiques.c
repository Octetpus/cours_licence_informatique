#include <stdio.h>

int nbAnnees(int nbDebut, int seuil)
{
    int nbMoustiques=nbDebut;
    int annees=0;
    printf("Calcul en cours...\n");
    while (nbMoustiques<seuil)
    {
        nbMoustiques=nbMoustiques/2+50;
        annees++;
        printf("%d\n", nbMoustiques);
    }
    return annees;
}

int nbAnneesAvecInterruptionSiPointFixe(int nbDebut, int seuil)
{
    int nbMoustiques=nbDebut;
    int nbMoustiquesPrecedemment;
    int PointFixe=0;
    int annees=0;
    printf("Calcul en cours...\n");
    while ((nbMoustiques<seuil)&&(!PointFixe))
    {
        nbMoustiquesPrecedemment=nbMoustiques;
        nbMoustiques=nbMoustiques/2+50;
        annees++;
        printf("%d\n", nbMoustiques);
        if (nbMoustiquesPrecedemment==nbMoustiques)
        {
            PointFixe=1;
            printf("Nous avons atteint le point fixe. Le nombre de moustiques restera toujours à %d.\n", nbMoustiques);
        }
    }
    return annees;
}

int main()
{
    int n=0;
    int initial=0;
    int cible=0;
    
    printf("Avec combien de moustiques voulez-vous commencer?");
    scanf("%d", &initial);
    
    printf("Combien de moustiques visez-vous?");
    scanf("%d", &cible);
    
    n=nbAnnees(initial, cible);
    //n=nbAnneesAvecInterruptionSiPointFixe(initial, cible);
    printf("Il vous faudra %d annees.\n", n);
    
    return 0;
    
}


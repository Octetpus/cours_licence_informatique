#include <stdio.h>

int main(){
    char lettre_a='a'; // stocke dans la variable lettre_a le code ASCII de 'a', donc 97
    int i; // declaration du futur indice de boucle
    for (i=0; i<26; i++) // Pour i allant de 0 à 25, par pas de 1
    {
        printf("%d-ieme lettre: %c \n", i, lettre_a+i);
        /* le premier "trou" (%d) est complété par i,
         le 2e "trou" (%c) est complété par l'interprétation en caractère du code ASCII 97+i */
    }
    return 0;
}

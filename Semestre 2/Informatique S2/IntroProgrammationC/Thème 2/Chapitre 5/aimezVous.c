#include <stdio.h>

int main(){
    char reponse;
    // pour une fois, on ne fera pas d'"initialisaiton par sécurité" avant le scanf
    do
    {
        printf("\n Aimez-vous la programmation ? ");
        printf("Répondre 'o' pour oui, ou 'n' pour non. ");
        scanf(" %c", &reponse);// saisie au clavier
        /* A cause d'une subtilité d'utilisation de scanf avec le code spécial %c, on a besoin d'un espace dans
         les guillemets avant %c. La raison de cette subtilité est hors programme dans ce module */
    }
    while ((reponse!='o') && (reponse != 'n')); // do...while -> faire... tant que la condition est vraie
    
    // quand on arrive là, on sait que réponse vaut soit 'o' soit 'n'
    if (reponse=='o')
    {
        printf("Super :-) \n");
    }
    else // on sait alors que reponse vaut 'n'
    {
        printf("Dommage :-( \n");
    }
    
    return 0;
}

#include <stdio.h>

int main(){
    float somme=0; // déclaration et initialisation
    float poids=0; // déclaration (et initialisation par sécurité)
    printf("Vous avez droit à une valise de 23kg.\n");
    printf("Elle est initialement vide.\n");
    while (somme<=23) // Tant que la somme est inf ou égale à 23, on entre dans la boucle
    {
        printf("Il reste %f kg.\n", 23-somme);
        printf("Combien de kg de vêtements voulez-vous ajouter ? ");
        scanf("%f", &poids); // saisie au clavier
        somme=somme+poids; // ajout du poids à la somme existante
    }
    printf("Valise trop pleine!\n");
    return 0;
}

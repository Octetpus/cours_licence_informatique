#include <stdio.h>

int main(){
    
    int continuer=1; /* booléen valant 1 pour
                      continuer et 0 pour stop*/
    while (continuer)
    {
        printf("Coucou!\n");
        
    }
    // On ne sort jamais de la boucle car la variable continuer reste toujours à vrai!
    return 0;
}

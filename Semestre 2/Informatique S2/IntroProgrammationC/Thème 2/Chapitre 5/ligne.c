#include <stdio.h>

int main(){
    int i; // declaration du futur indice de boucle
    int n=0;
    printf("Combien de lignes voulez-vous? ");
    scanf("%d", &n); // saisie au clavier d'un entier
    for (i=0; i<n; i++) // pour allant de 0 à n-1, incrémenté d'un cran à chaque étape
    {
        printf("Ligne numéro %d \n", i);
    }
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
    int mystere ; //sera le nb mystere
    int prop=0;; // sera la proposition du joueur
    int compteur; // compte le nombre de propositions faites
    srand(time(NULL)); // initialisation de l'aléatoire
    mystere=(rand() % 101); // un nombre aléatoire entre 0 et 100
    printf("Devinez le nombre mystère entre 0 et 100.\n");
    printf("Vous avez 5 essais.\n");
    compteur=1;
    do
    {
        printf("Proposition %d : ", compteur);
        scanf("%d", &prop); // saisie au clavier
        compteur++; // incrementation du compteur de proposition
        if (prop>mystere)
        {
            printf("C'est moins.\n");
        }
        else if (prop<mystere)
        {
            printf("C'est plus.\n");
        }
    } while (prop!=mystere && compteur<=5); /* il faut répéter tant que le nombre mystere
                                             n'a pas ete trouve et que le nombre de propositions
                                             n'a pas été dépassé */
    if (prop==mystere)
    {
        printf("C'est gagné!");
    }
    else // on sait que le compteur a dépassé
    {
        printf("Raté! Le nombre mystere était: %d \n", mystere);
    }
    return 0;
    
}

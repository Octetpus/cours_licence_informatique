# Cours de licence informatique

Treeeeee

```
├── README.md
├── Semestre 1
│   ├── Informatique S1
│   │   ├── BaseNumeration
│   │   │   ├── Chapitre 0
│   │   │   │   ├── poly_Bases_num_2018_chap_histoire.pdf
│   │   │   │   └── Slides_Bases_Num_Histoire.pdf
│   │   │   ├── Chapitre 1
│   │   │   │   ├── Bases_num_td1.pdf
│   │   │   │   └── poly_Bases_num_2018_chap_entiers.pdf
│   │   │   ├── Chapitre 2
│   │   │   │   ├── Bases_num_td2.pdf
│   │   │   │   └── poly_Bases_num_2018_chap_flottants_repro.pdf
│   │   │   ├── Chapitre 3
│   │   │   │   ├── Bases_num_td3.pdf
│   │   │   │   └── poly_Bases_num_2018_chap_ascii_hamming_repro.pdf
│   │   │   ├── Chapitre 4
│   │   │   │   ├── Bases_num_td4.pdf
│   │   │   │   └── poly_Bases_num_2018_chap_booleen_repro.pdf
│   │   │   ├── poly_Bases_num_2018_EnvoyeRepro.pdf
│   │   │   └── Sujet_examen_2017.pdf
│   │   ├── IntroAlgo
│   │   │   ├── chainesCaracteres.pdf
│   │   │   ├── CoursAlgoS1.pdf
│   │   │   ├── exam-S1-17-18.pdf
│   │   │   ├── exam-S1-ABJ-17-18.pdf
│   │   │   ├── Tableaux.pdf
│   │   │   └── TestConfitionelle.pdf
│   │   └── TPShell
│   │       ├── shell-comment-debuter.pdf
│   │       ├── tp-shell-1.pdf
│   │       ├── tp-shell-2.pdf
│   │       └── tp-shell-3.pdf
│   └── Mathématiques S1
│       ├── Annales
│       │   ├── 2017-11-07_Examen-Intermediaire-CORRIGE_2.pdf
│       │   ├── 2017-11-07_Examen-intermediaire_v4.pdf
│       │   ├── 2018-11-Substitution.pdf
│       │   ├── corrige-EI-20181106.pdf
│       │   ├── corrige-substitution-20181122.pdf
│       │   └── EI-2018-11-06.pdf
│       ├── Chapitre 1
│       │   ├── 2018_S1_chapitre_1.pdf
│       │   ├── S_1_beamer_compagnon_chapitre_1.pdf
│       │   ├── TD1-2018-2019.pdf
│       │   └── TD2Etudiants Version 3.pdf
│       ├── Chapitre 2
│       │   ├── 2018_S1_chapitre_2.pdf
│       │   ├── S_1_beamer_compagnon_chapitre_2.pdf
│       │   ├── TD3 Exercices limite-2018-19-V3.pdf
│       │   └── TD4_Etudiants.pdf
│       └── Chapitre 3
│           ├── 2018_S1_chapitre_3.pdf
│           ├── S_1_beamer_compagnon_chapitre_3.pdf
│           └── TD5-nombres-complexes-v3.pdf
└── Semestre 2
    ├── Informatique S2
    │   ├── Algo1
    │   │   ├── Thème 1
    │   │   │   ├── Catalogue dalgo.pdf
    │   │   │   ├── slidesALGO1Chap2.pdf
    │   │   │   ├── slidesALGO1CIntroChap1.pdf
    │   │   │   ├── slidesALGOChap3AnalyseAlgo.pdf
    │   │   │   └── TD01 analyse complexite preuves.pdf
    │   │   └── Thème 2
    │   │       ├── slidesALGO1Chap4Recursivite.pdf
    │   │       └── TD02 recursivite.pdf
    │   └── IntroProgrammationC
    │       ├── Divers
    │       │   ├── cm-revisions-v2.pdf
    │       │   ├── ExamMineure.pdf
    │       │   ├── MessageErreur.pdf
    │       │   ├── NoticeTP.pdf
    │       │   ├── Sujet TP noté Majeure I5b 2018.pdf
    │       │   └── Sujet TP noté Mineure 2018.pdf
    │       ├── Thème 1
    │       │   ├── Chapitre 1
    │       │   │   ├── hello.c
    │       │   │   └── slidesProgCIntroChap1.pdf
    │       │   ├── Chapitre 2
    │       │   │   ├── prefere.c
    │       │   │   └── slidesProgCChap2.pdf
    │       │   ├── Chapitre 3
    │       │   │   ├── prefereSigne.c
    │       │   │   └── slidesProgCChap3.pdf
    │       │   ├── Correction TD01-20190322.zip
    │       │   ├── Correction TP1-20190322.zip
    │       │   ├── TD-TP01 if - switch.pdf
    │       │   └── verifierResultat.txt
    │       ├── Thème 2
    │       │   ├── Chapitre 4
    │       │   │   └── slidesProgCChap4.pdf
    │       │   ├── Chapitre 5
    │       │   │   ├── aimezVous.c
    │       │   │   ├── alphabet.c
    │       │   │   ├── coucou.c
    │       │   │   ├── ligne.c
    │       │   │   ├── nbMystere.c
    │       │   │   ├── slidesProgCChap5.pdf
    │       │   │   └── valise.c
    │       │   ├── Corrigés Fiche TP 02-20190322.zip
    │       │   ├── diagonale.c
    │       │   ├── exoMoustiques.c
    │       │   ├── geometrieRectangle.c
    │       │   └── TD-TP02 fonctions for while.pdf
    │       ├── Thème 3
    │       │   ├── ComplementTP-testing.pdf
    │       │   ├── td03.c
    │       │   ├── TD-TP03 tableaux.pdf
    │       │   └── tp03.c
    │       ├── Thème 4
    │       │   ├── argExecutable.c
    │       │   ├── Chapitre 6
    │       │   │   ├── inventaireProduits.c
    │       │   │   └── slidesProgCChap6.pdf
    │       │   ├── Chapitre 7
    │       │   │   ├── argExecutable.c
    │       │   │   ├── chaine-bonjour.c
    │       │   │   ├── conversionFloat.c
    │       │   │   ├── conversionInt.c
    │       │   │   ├── fonctionsStr.c
    │       │   │   ├── slidesProgCChap7.pdf
    │       │   │   ├── sommeArgvFloat.c
    │       │   │   ├── sommeArgvInt.c
    │       │   │   └── ville.c
    │       │   ├── Correction TP04
    │       │   │   ├── argExecutableCorrige.c
    │       │   │   ├── footCorrige.c
    │       │   │   ├── nomAgetp04Exo5.c
    │       │   │   ├── stringTimeTp04Exo7.c
    │       │   │   ├── tp04Exo2.c
    │       │   │   └── tp04Exo3-6.c
    │       │   ├── foot.c
    │       │   ├── td04.c
    │       │   └── TD-TP04 tableaux 2D string.pdf
    │       └── Thème 5
    │           ├── Chapitre 8
    │           │   ├── coordonnees.c
    │           │   ├── demoPointeur.c
    │           │   ├── etudiant.c
    │           │   ├── jeuVersion1.c
    │           │   ├── jeuVersion2.c
    │           │   ├── jeuVersion3.c
    │           │   ├── lectureString.c
    │           │   ├── lectureTab2D.c
    │           │   ├── personne.c
    │           │   ├── QCMAllocDyn.c
    │           │   ├── QCMPassageParAdresse.c
    │           │   ├── slidesProgCChap8.pdf
    │           │   └── tableau123.c
    │           ├── Correction TP05
    │           │   ├── plateau.c
    │           │   ├── produit.c
    │           │   ├── score.c
    │           │   └── tabMalloc.c
    │           ├── Correction TP06
    │           │   ├── listeChaineCirculaire.c
    │           │   ├── listeDoublementChaineeCirculaire.c
    │           │   ├── listes.c
    │           │   ├── listesCopieEtPointeurs.c
    │           │   ├── listesReels.c
    │           │   └── mallocAlea.c
    │           ├── td05.c
    │           ├── TD-TP05 structures-pointeurs.pdf
    │           └── TD-TP06 listes.pdf
    └── MAAS
        ├── Fiche_1_2018.pdf
        ├── Fiche_2.pdf
        ├── Fiche_3_AVEC_Maths.pdf
        ├── TD_1.pdf
        ├── TD_2.pdf
        ├── TD_3_Avec_Maths.pdf
        └── TD_3_Sans_Maths.pdf
```
